package de.superioz.susanoo.module.spec;

import java.io.Serializable;

/**
 * Created on 28.05.2016.
 */
public class RangeValue implements Serializable, ConfigValue {

	int from, to, def, curr, diff;
	String pattern;

	public RangeValue(int from, int to, int def, int diff, String pattern){
		this.from = from;
		this.to = to;
		this.def = def;
		this.curr = def;
		this.diff = diff;
		this.pattern = pattern;
	}

	public RangeValue(int from, int to, int def, int diff){
		this(from, to, def, diff, "%val");
	}

	public String getText(int val){
		return pattern.replace("%val", val + "");
	}

	public int getFrom(){
		return from;
	}

	public int getTo(){
		return to;
	}

	public int getDefault(){
		return def;
	}

	public int getDef(){
		return def;
	}

	public void setDef(int def){
		this.def = def;
	}

	public void setCurr(int curr){
		this.curr = curr;
	}

	public int getCurrent(){
		return curr;
	}

	public void getCurrent(int curr){
		this.curr = curr;
	}

	public int getDiff(){
		return diff;
	}
}
