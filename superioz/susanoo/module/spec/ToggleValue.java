package de.superioz.susanoo.module.spec;

import java.io.Serializable;

/**
 * Created on 29.05.2016.
 */
public class ToggleValue implements Serializable, ConfigValue
{
    private boolean state;
    private boolean def;

    public ToggleValue(boolean def)
    {
        this.state = def;
        this.def = def;
    }

    public boolean getState()
    {
        return state;
    }

    public void setState(boolean state)
    {
        this.state = state;
    }

    public boolean getDefault()
    {
        return def;
    }

    public void setDefault(boolean def)
    {
        this.def = def;
    }
}
