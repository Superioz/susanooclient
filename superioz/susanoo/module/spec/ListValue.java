package de.superioz.susanoo.module.spec;

import java.io.Serializable;
import java.util.List;

/**
 * Created on 29.05.2016.
 */
public class ListValue implements Serializable, ConfigValue {

	private List<String> values;

	private String current;
	private int currentIndex;

	public ListValue(List<String> values, int defaultIndex){
		this.values = values;
		this.currentIndex = defaultIndex;
	}

	public ListValue(List<String> values){
		this(values, 0);
	}

	/**
	 * Get the next object
	 *
	 * @return Stringhe object
	 */
	public String next(){
		if(currentIndex == values.size() - 1){
			currentIndex = 0;
		}
		else{
			currentIndex++;
		}

		current = values.get(currentIndex);
		return current;
	}

	/**
	 * Jump to given index
	 *
	 * @param index Stringhe index
	 * @return Stringhe object
	 */
	public String jump(int index){
		if(index >= values.size() || index < 0){
			index = 0;
		}
		currentIndex = index;

		current = values.get(currentIndex);
		return current;
	}

	/**
	 * Get the current object
	 *
	 * @return Stringhe object
	 */
	public String get(){
		if(current == null) current = values.get(currentIndex);
		return current;
	}

	// Intern

	public List<String> getValues(){
		return values;
	}

	public void setValues(List<String> values){
		this.values = values;
	}

	public int getCurrentIndex(){
		return currentIndex;
	}

	public void setCurrentIndex(int currentIndex){
		this.currentIndex = currentIndex;
	}
}
