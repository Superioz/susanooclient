package de.superioz.susanoo.module.spec;

import de.superioz.susanoo.util.impl.SerializablePair;

import java.util.HashMap;

/**
 * Created on 29.05.2016.
 */
public interface ConfigSaveable {

	HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues();

	void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values);
}
