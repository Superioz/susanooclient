package de.superioz.susanoo.module;

/**
 * Created on 26.05.2016.
 */
public enum ModuleCategory
{
    RENDER(0xff9fff80),
    COMBAT(0xffe62e00),
    MOVEMENT(0xff1a8cff),
    OTHER(0xffbf00ff),
    WORLD(0xffffcc00);

    private int color;

    ModuleCategory(int color)
    {
        this.color = color;
    }

    public int getColor()
    {
        return color;
    }

    public String getName()
    {
        String n = name().toLowerCase();
        return n.substring(0, 1).toUpperCase() + n.substring(1);
    }
}
