package de.superioz.susanoo.module;

import de.superioz.susanoo.Client;
import de.superioz.susanoo.event.EventHandler;
import de.superioz.susanoo.event.Listener;
import de.superioz.susanoo.event.events.ClientRenderEvent;
import de.superioz.susanoo.event.events.ClientUpdateEvent;
import de.superioz.susanoo.event.events.KeyPressEvent;
import de.superioz.susanoo.module.mods.combat.*;
import de.superioz.susanoo.module.mods.movement.*;
import de.superioz.susanoo.module.mods.other.*;
import de.superioz.susanoo.module.mods.render.*;
import de.superioz.susanoo.module.mods.world.*;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 27.05.2016.
 */
public class ModuleManager implements Listener {

	private static final List<Module> MODULES = new ArrayList<Module>();
	private static boolean debugInfos = false;

	/**
	 * Registers all modules
	 */
	public static void startup(){
		// Render
		registerModule(new Fullbright());
		registerModule(new EntityESP());
		registerModule(new ChestESP());
		registerModule(new PlayerESP());
		registerModule(new Xray());
		registerModule(new Trajectories());

		// Movement
		registerModule(new AntiKnockback());
		registerModule(new SafeWalk());
		registerModule(new Speed());
		registerModule(new Glide());
		registerModule(new Flight());
		registerModule(new NoFall());
		registerModule(new AutoMovement());
		registerModule(new FastLadder());
		registerModule(new Spider());

		// World
		registerModule(new Tower());
		registerModule(new Nuker());
		registerModule(new FastPlace());
		registerModule(new AutoMine());
		registerModule(new AutoFish());

		// Other
		registerModule(new ArrowPredictor());
		registerModule(new ChestStealer());
		registerModule(new FastRespawn());
		registerModule(new SafePlay());
		registerModule(new Test());

		// Combat
		registerModule(new AutoSoup());
		registerModule(new Criticals());
		registerModule(new AutoBlock());
		registerModule(new TriggerBot());
		registerModule(new SmoothAimbot());
	}

	/**
	 * Closes all hacks
	 */
	public static void shutdown(){
		for(Module m : MODULES){
			if(m.isActive()){
				m.setActive(false);
				m.activeButton.setState(m.isActive());
			}
		}
	}

	/**
	 * Checks if module with given keycode exists
	 *
	 * @param keycode The keycode
	 * @return The result
	 */
	public static boolean hasModule(int keycode){
		return getModule(keycode) != null;
	}

	/**
	 * Gets the module with given keycode
	 *
	 * @param keyCode The keycode
	 * @return The module
	 */
	public static Module getModule(int keyCode){
		for(Module m : getModules()){
			if(m.isKeyCode(keyCode)){
				return m;
			}
		}

		return null;
	}

	/**
	 * Get module from clazz
	 *
	 * @param clazz The clazz
	 * @return The module
	 */
	public static Module getModule(Class<? extends Module> clazz){
		for(Module m : getModules()){
			if(m.getClass().equals(clazz)){
				return m;
			}
		}

		return getModules().get(0);
	}

	public static List<Module> getModules(ModuleCategory category, boolean singleplayer){
		List<Module> modules = new ArrayList<Module>();

		for(Module m : getModules()){
			if(category == null || m.getCategory() == category){
				if(singleplayer && m.isSingleplayerBased()
						|| !singleplayer && !m.isSingleplayerBased()){
					modules.add(m);
				}
			}
		}

		return modules;
	}

	@EventHandler
	public static void onKeyPress(KeyPressEvent event){
		if(!Client.options.activate.getState()){
			return;
		}
		if(event.getKeyCode() == Keyboard.KEY_F3){
			debugInfos = !debugInfos;
			Client.options.hud.setState(!debugInfos);
		}

		for(Module m : ModuleManager.getModules()){
			if(m.isKeyCode(event.getKeyCode())){
				m.toggle(false);
			}
		}
	}

	@EventHandler
	public static void onUpdate(ClientUpdateEvent event){
		if(!Client.options.activate.getState()){
			return;
		}

		for(Module m : ModuleManager.getModules()){
			if(m.isActive()){
				m.onUpdate();
			}
		}
	}

	@EventHandler
	public static void onRender(ClientRenderEvent event){
		if(!Client.options.activate.getState()){
			return;
		}

		for(Module m : ModuleManager.getModules()){
			if(m.isActive()){
				m.onRender(event.getPartialTicks());
			}
		}
	}

	public static void registerModule(Module module){
		if(MODULES.contains(module)){
			return;
		}

		MODULES.add(module);
	}

	public static List<Module> getModules(){
		return MODULES;
	}

	// ======================

}
