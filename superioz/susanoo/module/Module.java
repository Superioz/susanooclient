package de.superioz.susanoo.module;

import de.superioz.susanoo.Client;
import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.gui.obj.GuiToggleButton;
import de.superioz.susanoo.module.spec.IngameStated;
import de.superioz.susanoo.module.spec.SingleplayerBased;
import de.superioz.susanoo.module.spec.ToggleValue;
import de.superioz.susanoo.util.Colors;
import de.superioz.susanoo.util.impl.Consumer;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Keyboard;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created on 25.05.2016.
 */
public abstract class Module {

	protected static Minecraft minecraft = Client.mc;

	protected String name;
	protected int keyCode;
	protected ToggleValue active = new ToggleValue(false);
	protected ModuleCategory category;

	protected GuiToggleButton activeButton;
	protected JButton keyCodeButton;

	/**
	 * Constructor
	 *
	 * @param name     The name of the module (killaura, ..)
	 * @param keyCode  The default keycode (0 for NONE)
	 * @param category The category
	 */
	public Module(String name, int keyCode, ModuleCategory category){
		this.name = name;
		this.keyCode = keyCode;
		this.category = category;
	}

	// Events

	public abstract void onEnable();

	public abstract void onDisable();

	public abstract void onRender(float partialTicks);

	public abstract void onUpdate();

	public abstract void onEvent(Event event);

	// Events

	/**
	 * Toggles the module
	 */
	public boolean toggle(boolean fromGUI){
		if(!Client.mc.isSingleplayer() && isSingleplayerBased()){
			Client.sendMessage("\247cThis Module is only singleplayer safe! (" + name + ")");
			return false;
		}
		else if(fromGUI && this instanceof IngameStated){
			Client.sendMessage("\247cThis Module can only be toggled ingame! (" + name + ")");
			return false;
		}

		// State
		setActive(!isActive());

		if(isActive()){
			onEnable();
		}
		else{
			onDisable();
		}

		//
		if(!fromGUI){
			this.activeButton.setState(isActive());
		}

		return true;
	}

	public void updateButton(boolean fromGUI){
		if(!fromGUI){
			activeButton.setState(isActive());
		}
	}

	/**
	 * Get the button for toggling the active mode
	 *
	 * @return The toggle button object
	 */
	public GuiToggleButton getActiveToggleButton(){
		final JToggleButton base = new JToggleButton();
		base.setFocusable(false);
		this.activeButton = new GuiToggleButton(base, active,
				"Activated", "Deactivated", Colors.GREEN, Colors.RED,
				new Consumer<GuiToggleButton.WrappedActionEvent>() {
					@Override
					public void consume(GuiToggleButton.WrappedActionEvent event){
						base.setSelected(false);
						if(!toggle(true)){
							event.setCancelled(true);
						}
					}
				});
		return activeButton;
	}

	/**
	 * Get the button for change the keycode
	 *
	 * @return The button
	 */
	public JButton getKeyCodeButton(){
		keyCodeButton = new JButton(Keyboard.getKeyName(getKeyCode()));
		keyCodeButton.setForeground(Colors.BLACK);
		keyCodeButton.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e){
				//
			}

			@Override
			public void keyPressed(KeyEvent e){
				//
			}

			@Override
			public void keyReleased(KeyEvent e){
				int keyCode = Keyboard.getKeyIndex((e.getKeyChar() + "").toUpperCase());
				keyCodeButton.setText(Keyboard.getKeyName(keyCode));

				keyCodeButton.setForeground(Colors.BLACK);
				setKeyCode(keyCode);
			}
		});
		return keyCodeButton;
	}

	//
	// ================================
	//

	public boolean isSingleplayerBased(){
		return this instanceof SingleplayerBased;
	}

	// Intern methods

	public ModuleCategory getCategory(){
		return category;
	}

	public void setCategory(ModuleCategory category){
		this.category = category;
	}

	public String getName(){
		return name;
	}

	public int getKeyCode(){
		return keyCode;
	}

	public boolean isKeyCode(int k){
		return getKeyCode() != -1 && getKeyCode() == k;
	}

	public void setKeyCode(int keyCode){
		this.keyCode = keyCode;
	}

	public boolean isActive(){
		return active.getState() && Client.isInGame();
	}

	public void setActive(boolean active){
		this.active.setState(active);
	}
}
