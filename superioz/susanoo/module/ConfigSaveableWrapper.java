package de.superioz.susanoo.module;

import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.util.impl.SerializablePair;

import java.util.HashMap;

/**
 * Created on 22.06.2016.
 */
public class ConfigSaveableWrapper {

	private ConfigValue configValue;
	private String name;
	private int id;

	public ConfigSaveableWrapper(ConfigValue configSaveable, String name, int id){
		this.configValue = configSaveable;
		this.name = name;
		this.id = id;
	}

	/**
	 * Builds the map
	 *
	 * @param wrappers The wrappers
	 * @return The map
	 */
	public static HashMap<Integer, SerializablePair<String, ConfigValue>> buildMap(ConfigSaveableWrapper... wrappers){
		HashMap<Integer, SerializablePair<String, ConfigValue>> map = new HashMap<Integer, SerializablePair<String, ConfigValue>>();

		for(ConfigSaveableWrapper wrapper : wrappers){
			map.put(wrapper.getId(), new SerializablePair<String, ConfigValue>(wrapper.getName(), wrapper.getConfigValue()));
		}
		return map;
	}

	public ConfigValue getConfigValue(){
		return configValue;
	}

	public void setConfigValue(ConfigValue configValue){
		this.configValue = configValue;
	}

	public String getName(){
		return name;
	}

	public void setName(String name){
		this.name = name;
	}

	public int getId(){
		return id;
	}

	public void setId(int id){
		this.id = id;
	}

}
