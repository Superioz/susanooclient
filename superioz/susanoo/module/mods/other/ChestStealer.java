package de.superioz.susanoo.module.mods.other;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.ConfigSaveableWrapper;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.module.spec.RangeValue;
import de.superioz.susanoo.module.spec.ToggleValue;
import de.superioz.susanoo.util.impl.SerializablePair;
import de.superioz.susanoo.util.Timer;
import net.minecraft.inventory.ContainerChest;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created on 01.06.2016.
 */
public class ChestStealer extends Module implements ConfigSaveable {

	private Timer time = new Timer();

	private RangeValue delay = new RangeValue(0, 200, 40, 1, "%val ms");
	private ToggleValue autoClose = new ToggleValue(false);

	/**
	 * Constructor
	 */
	public ChestStealer(){
		super("cheststealer", Keyboard.KEY_NONE, ModuleCategory.OTHER);
	}

	@Override
	public void onEnable(){
	}

	@Override
	public void onDisable(){
	}

	@Override
	public void onRender(float partialTicks){
	}

	@Override
	public void onUpdate(){
		if((minecraft.thePlayer.openContainer != null) &&
				((minecraft.thePlayer.openContainer instanceof ContainerChest))){
			ContainerChest container = (ContainerChest) minecraft.thePlayer.openContainer;
			//
			Random r = new Random();
			int size = container.getLowerChestInventory().getSizeInventory();
			List<Integer> fields = new ArrayList<Integer>(size);

			for(int i = 0; ; i = r.nextInt(size)){
				if(fields.size() >= size){
					break;
				}

				if(!fields.contains(i)){
					fields.add(i);
				}
			}

			for(int i : fields){
				if((this.time.hasReached(delay.getCurrent()))
						&& container.getLowerChestInventory().getStackInSlot(i) != null){
					minecraft.playerController.windowClick(container.windowId, i, 0, 1, minecraft.thePlayer);
					this.time.reset();
				}
			}

			if(container.getInventory().isEmpty() && autoClose.getState()){
				minecraft.thePlayer.closeScreen();
			}
		}
	}

	@Override
	public void onEvent(Event event){
	}

	@Override
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues(){
		return ConfigSaveableWrapper.buildMap(
				new ConfigSaveableWrapper(delay, "Delay:", 0),
				new ConfigSaveableWrapper(autoClose, "Auto Close:", 1)
		);
	}

	@Override
	public void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		delay = (RangeValue) values.get(0).getSecond();
		autoClose = (ToggleValue) values.get(1).getSecond();
	}
}
