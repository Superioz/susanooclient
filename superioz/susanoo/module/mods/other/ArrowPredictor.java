package de.superioz.susanoo.module.mods.other;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.ConfigSaveableWrapper;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.module.spec.ListValue;
import de.superioz.susanoo.util.Utils;
import de.superioz.susanoo.util.impl.SerializablePair;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFence;
import net.minecraft.block.BlockFenceGate;
import net.minecraft.block.BlockWall;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import java.util.*;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 22.06.2016.
 */
public class ArrowPredictor extends Module implements ConfigSaveable {

	private static ListValue mode = new ListValue(Arrays.asList("Warning Mode", "Dodge Mode"), 0);
	public static List<EntityArrow> couldHitList = new ArrayList<>();

	/**
	 * Constructor
	 */
	public ArrowPredictor(){
		super("arrowpredict", Keyboard.KEY_NONE, ModuleCategory.OTHER);
	}

	@Override
	public void onEnable(){
		couldHitList.clear();
	}

	@Override
	public void onDisable(){
		couldHitList.clear();
	}

	@Override
	public void onRender(float partialTicks){
		List<Entity> entityList = mc.theWorld.loadedEntityList;
		List<EntityArrow> arrows = new ArrayList<>();

		for(Entity e : entityList){
			if(e instanceof EntityArrow) arrows.add((EntityArrow) e);
		}

		for(EntityArrow arrow : arrows){
			if(arrow.onGround || arrow.ticksInGround > 0 || arrow.isDead){
				continue;
			}

			double arrowMotionX = arrow.motionX,
					arrowMotionY = arrow.motionY,
					arrowMotionZ = arrow.motionZ;
			double arrowPosX = arrow.posX,
					arrowPosY = arrow.posY,
					arrowPosZ = arrow.posZ;
			double gravity = 0.05;
			RenderManager renderManager = mc.getRenderManager();

			if(mode.getCurrentIndex() == 1){
				for(int i = 0; i < 1000; i++){
					double x = arrowPosX - renderManager.renderPosX,
							y = arrowPosY - renderManager.renderPosY,
							z = arrowPosZ - renderManager.renderPosZ;
					GL11.glVertex3d(x, y, z);

					List<Entity> list = mc.theWorld.getEntitiesInAABBexcluding(null,
							new AxisAlignedBB(arrowPosX - 0.5, arrowPosY - 0.5, arrowPosZ - 0.5,
									arrowPosX + 0.5, arrowPosY + 0.5, arrowPosZ + 0.5), null);
					if(list.contains(mc.thePlayer)){
						List<BlockPos> availablePositions = new ArrayList<>();
						BlockPos playerPos = mc.thePlayer.getPosition();
						EnumFacing arrowFacing = arrow.getHorizontalFacing();

						if(arrowFacing == EnumFacing.NORTH
								|| arrowFacing == EnumFacing.SOUTH){
							availablePositions.add(playerPos.east());
							availablePositions.add(playerPos.west());
						}
						else if(arrowFacing == EnumFacing.EAST
								|| arrowFacing == EnumFacing.WEST){
							availablePositions.add(playerPos.north());
							availablePositions.add(playerPos.south());
						}

						List<BlockPos> newPositions = new ArrayList<>();
						for(BlockPos p : availablePositions){
							IBlockState state = mc.theWorld.getBlockState(p);
							Block block = state.getBlock();

							if(!block.getMaterial().isSolid()
									|| !block.getMaterial().isLiquid()
									&& !(block instanceof BlockWall)
									&& !(block instanceof BlockFence)
									&& !(block instanceof BlockFenceGate)){
								newPositions.add(p);
							}
						}

						if(!newPositions.isEmpty()){
							Random r = new Random();
							BlockPos newPos = newPositions.get(r.nextBoolean() ? 0 : newPositions.size() >= 1 ? 1 : 0);

							if(newPos.getX() > playerPos.getX()){
								mc.thePlayer.motionX = 0.34F;
							}
							if(newPos.getZ() > playerPos.getZ()){
								mc.thePlayer.motionZ = 0.34F;
							}
						}
					}

					arrowPosX += arrowMotionX * 0.1;
					arrowPosY += arrowMotionY * 0.1;
					arrowPosZ += arrowMotionZ * 0.1;
					arrowMotionX *= 0.999D;
					arrowMotionY *= 0.999D;
					arrowMotionZ *= 0.999D;
					arrowMotionY -= gravity * 0.1;
				}
			}
			else{
				if(Utils.wouldHitEntity(arrow, mc.thePlayer, true)){
					couldHitList.add(arrow);
				}
				else{
					if(couldHitList.contains(arrow)){
						couldHitList.remove(arrow);
					}
				}
			}
		}
	}

	@Override
	public void onUpdate(){

	}

	@Override
	public void onEvent(Event event){

	}

	@Override
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues(){
		return ConfigSaveableWrapper.buildMap(
				new ConfigSaveableWrapper(mode, "Mode:", 0)
		);
	}

	@Override
	public void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		mode = (ListValue) values.get(0).getSecond();
	}
}
