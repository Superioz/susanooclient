package de.superioz.susanoo.module.mods.other;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import org.lwjgl.input.Keyboard;

/**
 * Created on 02.06.2016.
 */
public class FastRespawn extends Module {

    /**
     * Constructor
     */
    public FastRespawn(){
        super("fastrespawn", Keyboard.KEY_NONE, ModuleCategory.OTHER);
    }

    @Override
    public void onEnable(){
    }

    @Override
    public void onDisable(){
    }

    @Override
    public void onRender(float partialTicks){
    }

    @Override
    public void onUpdate(){
        if(!minecraft.thePlayer.isEntityAlive()){
            minecraft.thePlayer.respawnPlayer();
        }
    }

    @Override
    public void onEvent(Event event){
    }
}
