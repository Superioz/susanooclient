package de.superioz.susanoo.module.mods.other;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.gui.notification.Notification;
import de.superioz.susanoo.gui.notification.NotificationCenter;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import org.lwjgl.input.Keyboard;

/**
 * Created on 21.06.2016.
 */
public class Test extends Module {

	/**
	 * Constructor
	 */
	public Test(){
		super("test", Keyboard.KEY_NONE, ModuleCategory.OTHER);
	}

	@Override
	public void onEnable(){
		//
		NotificationCenter.addMessage(new Notification("\247bTest-Mode \2477activated."));
	}

	@Override
	public void onDisable(){
		//
	}

	@Override
	public void onRender(float partialTicks){
		//
	}

	@Override
	public void onUpdate(){
		//
	}

	@Override
	public void onEvent(Event event){
		//
	}
}
