package de.superioz.susanoo.module.mods.other;

import de.superioz.susanoo.Client;
import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.ConfigSaveableWrapper;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import de.superioz.susanoo.module.ModuleManager;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.module.spec.RangeValue;
import de.superioz.susanoo.util.impl.SerializablePair;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import org.lwjgl.input.Keyboard;

import java.util.HashMap;

/**
 * Created on 19.06.2016.
 */
public class SafePlay extends Module implements ConfigSaveable {

	public static RangeValue players = new RangeValue(5, 15, 5, 1);

	/**
	 * Constructor
	 */
	public SafePlay(){
		super("safeplay", Keyboard.KEY_NONE, ModuleCategory.OTHER);
	}

	@Override
	public void onEnable(){
	}

	@Override
	public void onDisable(){
	}

	@Override
	public void onRender(float partialTicks){
	}

	@Override
	public void onUpdate(){
		int counter = 0;

		for(Entity e : minecraft.theWorld.loadedEntityList){
			if(e instanceof EntityPlayer){
				EntityPlayer other = (EntityPlayer) e;
				double reach = minecraft.playerController.getBlockReachDistance();

				if(other.getDistanceSqToEntity(minecraft.thePlayer) < reach * reach
						&& !other.isInvisible()
						&& !other.isSpectator()
						&& other.getHealth() != 0F){
					counter++;
				}
			}
		}

		int mCounter = 0;
		if(counter > players.getCurrent()){
			for(Module m : ModuleManager.getModules()){
				if(m.getCategory() == ModuleCategory.COMBAT || m.getCategory() == ModuleCategory.MOVEMENT
						|| m.getCategory() == ModuleCategory.WORLD){
					if(m.isActive()){
						m.toggle(false);
						mCounter++;
					}
				}
			}
		}

		if(mCounter >= 1){
			Client.sendMessage("\2479Too much player around you. I deactivated some mods for you.");
		}
	}

	@Override
	public void onEvent(Event event){
	}

	@Override
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues(){
		return ConfigSaveableWrapper.buildMap(
				new ConfigSaveableWrapper(players, "Max Player Near:", 0)
		);
	}

	@Override
	public void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		if(values == null){
			return;
		}

		players = (RangeValue) values.get(0).getSecond();
	}
}
