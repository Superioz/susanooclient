package de.superioz.susanoo.module.mods.render;


import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.ConfigSaveableWrapper;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.module.spec.ListValue;
import de.superioz.susanoo.module.spec.ToggleValue;
import de.superioz.susanoo.util.impl.SerializablePair;
import de.superioz.susanoo.util.render.RenderUtil;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.item.*;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 21.06.2016.
 */
public class Trajectories extends Module implements ConfigSaveable {

	public static ListValue mode = new ListValue(Arrays.asList("First Person", "Third Person", "First & Third Person"), 0);
	public static ListValue target = new ListValue(Arrays.asList("Everything", "Only Arrow"), 0);
	public static ToggleValue highlightTarget = new ToggleValue(false);

	public static List<Integer> entityStack = new ArrayList<>();

	/**
	 * Constructor
	 */
	public Trajectories(){
		super("trajectories", Keyboard.KEY_NONE, ModuleCategory.RENDER);
	}

	@Override
	public void onEnable(){

	}

	@Override
	public void onDisable(){

	}

	@Override
	public void onRender(float partialTicks){
		EntityPlayerSP player = mc.thePlayer;

		if(mode.getCurrentIndex() == 0 || mode.getCurrentIndex() == 2){
			// Check if player is holding an item
			ItemStack stack = player.inventory.getCurrentItem();
			if(stack == null){
				return;
			}

			// Is this item throwable?
			Item item = stack.getItem();
			if(!(item instanceof ItemBow || item instanceof ItemSnowball
					|| item instanceof ItemEgg || item instanceof ItemEnderPearl
					|| item instanceof ItemPotion || item instanceof ItemFishingRod)){
				return;
			}

			if(!(item instanceof ItemBow) && target.getCurrentIndex() == 1){
				return;
			}
			RenderUtil.renderTrajectories(partialTicks, true);
		}
		if(mode.getCurrentIndex() == 2 || mode.getCurrentIndex() == 1){
			RenderUtil.renderTrajectories(partialTicks, false);
		}
	}

	/**
	 * Get entities within aabb
	 *
	 * @param aa The aa
	 * @return The entities
	 */
	public java.util.List<Entity> getEntitiesWithinAABB(AxisAlignedBB aa){
		java.util.List<Entity> entityList = new ArrayList<>();

		for(Entity e : mc.theWorld.getLoadedEntityList()){
			if(e.getBoundingBox().intersectsWith(aa)){
				entityList.add(e);
			}
		}
		return entityList;
	}

	@Override
	public void onUpdate(){

	}

	@Override
	public void onEvent(Event event){

	}

	@Override
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues(){
		return ConfigSaveableWrapper.buildMap(
				new ConfigSaveableWrapper(mode, "Mode:", 0),
				new ConfigSaveableWrapper(target, "Target:", 1),
				new ConfigSaveableWrapper(highlightTarget, "Highlight Target:", 2)
		);
	}

	@Override
	public void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		if(values == null){
			return;
		}

		mode = (ListValue) values.get(0).getSecond();
		target = (ListValue) values.get(1).getSecond();
		highlightTarget = (ToggleValue) values.get(2).getSecond();
	}
}
