package de.superioz.susanoo.module.mods.render;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.event.events.PlayerDieEvent;
import de.superioz.susanoo.module.*;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.module.spec.IngameStated;
import de.superioz.susanoo.module.spec.ToggleValue;
import de.superioz.susanoo.util.module.MinecraftBlockESP;
import de.superioz.susanoo.util.Colors;
import de.superioz.susanoo.util.impl.SerializablePair;
import net.minecraft.util.Tuple;
import org.lwjgl.input.Keyboard;

import java.util.Collections;
import java.util.HashMap;

/**
 * Created on 29.05.2016.
 */
public class ChestESP extends Module implements IngameStated, ConfigSaveable {

	public ToggleValue rewiChests = new ToggleValue(false);

	public static final MinecraftBlockESP BLOCK_ESP = new MinecraftBlockESP(Collections.singletonList(new Tuple<Integer, Integer>(168, 2)));

	/**
	 * Constructor
	 */
	public ChestESP(){
		super("chestesp", Keyboard.KEY_NONE, ModuleCategory.RENDER);
	}

	@Override
	public void onEnable(){
		minecraft.renderGlobal.loadRenderers();
	}

	@Override
	public void onDisable(){
		minecraft.renderGlobal.loadRenderers();
		BLOCK_ESP.onDie();
	}

	@Override
	public void onRender(float partialTicks){
		if(rewiChests.getState()){
			BLOCK_ESP.onRender(Colors.LIGHT_BLUE, 1.5f);
		}
	}

	@Override
	public void onUpdate(){
		//
	}

	@Override
	public void onEvent(Event event){
		if(event instanceof PlayerDieEvent){
			BLOCK_ESP.onDie();
		}
	}

	@Override
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues(){
		return ConfigSaveableWrapper.buildMap(
				new ConfigSaveableWrapper(rewiChests, "Rewi Chests:", 0)
		);
	}

	@Override
	public void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		if(values == null){
			return;
		}

		rewiChests = (ToggleValue) values.get(0).getSecond();
	}
}
