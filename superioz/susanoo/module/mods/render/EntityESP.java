package de.superioz.susanoo.module.mods.render;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import org.lwjgl.input.Keyboard;

/**
 * Created on 25.05.2016.
 */
public class EntityESP extends Module {

	public EntityESP(){
		super("entityesp", Keyboard.KEY_NONE, ModuleCategory.RENDER);
	}

	@Override
	public void onEnable(){
		//
	}

	@Override
	public void onDisable(){
		//
	}

	@Override
	public void onRender(float partialTicks){
	}

	@Override
	public void onUpdate(){
		//
	}

	@Override
	public void onEvent(Event event){
		//
	}
}
