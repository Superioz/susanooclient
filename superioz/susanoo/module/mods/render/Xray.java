package de.superioz.susanoo.module.mods.render;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.ConfigSaveableWrapper;
import de.superioz.susanoo.module.spec.IngameStated;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.module.spec.ToggleValue;
import de.superioz.susanoo.util.Colors;
import de.superioz.susanoo.util.impl.SerializablePair;
import de.superioz.susanoo.util.render.RenderUtil;
import net.minecraft.block.Block;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created on 29.05.2016.
 */
public class Xray extends Module implements IngameStated, ConfigSaveable {

	private static final List<Integer> BLOCKS = Arrays.asList(14, 15, 16, 21, 56, 73, 74, 129, 153);
	public static final List<BlockPos> TEST = new ArrayList<BlockPos>();

	public ToggleValue opacityMode = new ToggleValue(true);

	private float oldGamma = 0f;

	/**
	 * Constructor
	 */
	public Xray(){
		super("xray", Keyboard.KEY_NONE, ModuleCategory.RENDER);
	}

	@Override
	public void onEnable(){
		minecraft.renderGlobal.loadRenderers();
		oldGamma = minecraft.gameSettings.gammaSetting;
	}

	@Override
	public void onDisable(){
		minecraft.renderGlobal.loadRenderers();
		minecraft.gameSettings.gammaSetting = oldGamma;
	}

	@Override
	public void onRender(float partialTicks){
		List<BlockPos> posList = new ArrayList<BlockPos>();

		for(BlockPos p : TEST){
			posList.add(p);
		}

		for(BlockPos bp : posList){
			double rX = minecraft.getRenderManager().renderPosX;
			double rY = minecraft.getRenderManager().renderPosY;
			double rZ = minecraft.getRenderManager().renderPosZ;
			double minX = bp.getX() - rX, maxX = minX + 1;
			double minY = bp.getY() - rY, maxY = minY + 1;
			double minZ = bp.getZ() - rZ, maxZ = minZ + 1;
			AxisAlignedBB box = new AxisAlignedBB(minX, minY, minZ, maxX, maxY, maxZ);
			RenderUtil.fill(box, Colors.LIGHT_GREEN, Colors.WHITE, 1.5f);
		}
	}

	@Override
	public void onUpdate(){
		if(minecraft.gameSettings.gammaSetting < 16F){
			minecraft.gameSettings.gammaSetting += 0.5F;
		}
	}

	@Override
	public void onEvent(Event event){
		//
	}

	public static boolean isXrayBlock(int id){
		return BLOCKS.contains(id);
	}

	public static void update(BlockPos p){
		if(!TEST.contains(p) && TEST.size() < 100){
			TEST.add(p);
		}
		else if(TEST.contains(p)
				&& Block.getIdFromBlock(minecraft.theWorld.getBlockState(p).getBlock()) != 57){
			TEST.remove(p);
		}
	}

	@Override
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues(){
		return ConfigSaveableWrapper.buildMap(
				new ConfigSaveableWrapper(opacityMode, "Transparency Mode:", 0)
		);
	}

	@Override
	public void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		if(values == null){
			return;
		}

		opacityMode = (ToggleValue) values.get(0).getSecond();
	}
}
