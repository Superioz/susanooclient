package de.superioz.susanoo.module.mods.render;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import org.lwjgl.input.Keyboard;

/**
 * Created on 25.05.2016.
 */
public class Fullbright extends Module {

	private float oldSetting = 0F;

	public Fullbright(){
		super("fullbright", Keyboard.KEY_NONE, ModuleCategory.RENDER);
	}

	@Override
	public void onEnable(){
		oldSetting = minecraft.gameSettings.gammaSetting;
	}

	@Override
	public void onDisable(){
		minecraft.gameSettings.gammaSetting = oldSetting;
	}

	@Override
	public void onRender(float partialTicks){
		//
	}

	@Override
	public void onUpdate(){
		if(minecraft.gameSettings.gammaSetting < 16F){
			minecraft.gameSettings.gammaSetting += 0.5F;
		}
	}

	@Override
	public void onEvent(Event event){
		//
	}
}
