package de.superioz.susanoo.module.mods.render;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.event.events.PostRender3DEvent;
import de.superioz.susanoo.module.ConfigSaveableWrapper;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.module.spec.ToggleValue;
import de.superioz.susanoo.util.impl.SerializablePair;
import de.superioz.susanoo.util.render.NameTagManager;
import net.minecraft.entity.player.EntityPlayer;
import org.lwjgl.input.Keyboard;

import java.util.HashMap;

/**
 * Created on 29.05.2016.
 */
public class PlayerESP extends Module implements ConfigSaveable {

	public ToggleValue nametag = new ToggleValue(true);
	public ToggleValue health = new ToggleValue(true);
	public ToggleValue armor = new ToggleValue(true);

	/**
	 * Constructor
	 */
	public PlayerESP(){
		super("playeresp", Keyboard.KEY_NONE, ModuleCategory.RENDER);
	}

	@Override
	public void onEnable(){
		//
	}

	@Override
	public void onDisable(){
		//
	}

	@Override
	public void onRender(float partialTicks){
		//
	}

	@Override
	public void onUpdate(){
		//
	}

	@Override
	public void onEvent(Event event){
		if(event instanceof PostRender3DEvent
				&& nametag.getState()){
			for(Object o : minecraft.theWorld.playerEntities){
				EntityPlayer p = (EntityPlayer) o;

				if(p != minecraft.thePlayer && p.isEntityAlive()){
					minecraft.getRenderManager();
					//
					double ltpX = p.lastTickPosX, rX = minecraft.getRenderManager().renderPosX;
					double ltpY = p.lastTickPosY, rY = minecraft.getRenderManager().renderPosY;
					double ltpZ = p.lastTickPosZ, rZ = minecraft.getRenderManager().renderPosZ;
					float partialTicks = minecraft.timer.renderPartialTicks;
					double pX = ltpX + (p.posX - ltpX) * partialTicks - rX;
					double pY = ltpY + (p.posY - ltpY) * partialTicks - rY;
					double pZ = ltpZ + (p.posZ - ltpZ) * partialTicks - rZ;
					NameTagManager.renderNameTag(p, p.getDisplayName().getFormattedText(), pX, pY, pZ,
							armor.getState(), health.getState());
				}
			}
		}
	}

	@Override
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues(){
		return ConfigSaveableWrapper.buildMap(
				new ConfigSaveableWrapper(nametag, "Nametag:", 0),
				new ConfigSaveableWrapper(health, "Health:", 1),
				new ConfigSaveableWrapper(armor, "Armor:", 2)
		);
	}

	@Override
	public void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		if(values == null){
			return;
		}

		nametag = (ToggleValue) values.get(0).getSecond();
		health = (ToggleValue) values.get(1).getSecond();
		armor = (ToggleValue) values.get(2).getSecond();
	}
}
