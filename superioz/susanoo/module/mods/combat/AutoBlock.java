package de.superioz.susanoo.module.mods.combat;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.event.events.MouseClickEvent;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import org.lwjgl.input.Keyboard;

/**
 * Created on 01.06.2016.
 */
public class AutoBlock extends Module {

	/**
	 * Constructor
	 */
	public AutoBlock(){
		super("autoblock", Keyboard.KEY_NONE, ModuleCategory.COMBAT);
	}

	@Override
	public void onEnable(){
		//
	}

	@Override
	public void onDisable(){
		//
	}

	@Override
	public void onRender(float partialTicks){
		//
	}

	@Override
	public void onUpdate(){
		//
	}

	@Override
	public void onEvent(Event event){
		if(event instanceof MouseClickEvent){
			MouseClickEvent clickEvent = (MouseClickEvent) event;
			if(clickEvent.getType() != MouseClickEvent.Type.LEFTCLICK) return;

			ItemStack stack = minecraft.thePlayer.getCurrentEquippedItem();

			if((stack != null) && ((stack.getItem() instanceof ItemSword))){
				doBlock();
			}
		}
	}

	public void doBlock(){
		new Thread(new Runnable() {
			@Override
			public void run(){
				try{
					KeyBinding keybindUseItem = minecraft.gameSettings.keyBindUseItem;
					keybindUseItem.pressed = false;
					Thread.sleep(50L);
					keybindUseItem.pressed = true;
					Thread.sleep(50L);
					keybindUseItem.pressed = false;
				}
				catch(InterruptedException ex){
					// Nothing
				}
			}
		}).start();
	}
}
