package de.superioz.susanoo.module.mods.combat;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.friend.FriendManager;
import de.superioz.susanoo.module.ConfigSaveableWrapper;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.module.spec.RangeValue;
import de.superioz.susanoo.module.spec.ToggleValue;
import de.superioz.susanoo.util.impl.SerializablePair;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MovingObjectPosition;
import org.lwjgl.input.Keyboard;

import java.util.HashMap;
import java.util.Random;

/**
 * Created on 01.06.2016.
 */
public class TriggerBot extends Module implements ConfigSaveable {

	private Random r = new Random();
	private static long last = 0L;

	public ToggleValue entities = new ToggleValue(false);
	public RangeValue cps = new RangeValue(12, 12*6, 12, 1, "%val cps");
	public ToggleValue consistent = new ToggleValue(false);
	public ToggleValue ignoreFriends = new ToggleValue(true);

	/**
	 * Constructor
	 */
	public TriggerBot(){
		super("triggerbot", Keyboard.KEY_NONE, ModuleCategory.COMBAT);
	}

	@Override
	public void onEnable(){
	}

	@Override
	public void onDisable(){
	}

	@Override
	public void onRender(float partialTicks){
	}

	@Override
	public void onUpdate(){
		if((minecraft.currentScreen == null) &&
				(minecraft.objectMouseOver.typeOfHit == MovingObjectPosition.MovingObjectType.ENTITY)){
			Entity e = minecraft.objectMouseOver.entityHit;

			// Speed
			int minHits = 6;
			int maxHits = cps.getCurrent();
			if(consistent.getState() && maxHits > minHits * 2) minHits = maxHits - minHits;
			int randomSpeed = getRandomHitDelay(minHits, maxHits);

			if(e.isInvisible() || e.isDead || (ignoreFriends.getState() && FriendManager.isFriend(e))
					|| !(System.currentTimeMillis() - last > randomSpeed)){
				return;
			}

			if(e instanceof EntityPlayer || (e instanceof EntityLiving && entities.getState())){
				minecraft.clickMouse();
				last = System.currentTimeMillis();
			}
		}
	}

	@Override
	public void onEvent(Event event){
	}

	/**
	 * Get a random hit delay
	 *
	 * @param minHits The min hits
	 * @param maxHits The max hits
	 * @return The result
	 */
	private int getRandomHitDelay(int minHits, int maxHits){
		int minCpsMs = 1000 / minHits, maxCpsMs = 1000 / maxHits;

		return r.nextInt((minCpsMs - maxCpsMs) + 1) + maxCpsMs;
	}

	@Override
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues(){
		return ConfigSaveableWrapper.buildMap(
				new ConfigSaveableWrapper(entities, "Entities:", 0),
				new ConfigSaveableWrapper(cps, "Max Speed:", 1),
				new ConfigSaveableWrapper(consistent, "Consistent:", 2),
				new ConfigSaveableWrapper(ignoreFriends, "Ignore Friends:", 3)
		);
	}

	@Override
	public void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		if(values == null){
			return;
		}

		entities = (ToggleValue) values.get(0).getSecond();
		cps = (RangeValue) values.get(1).getSecond();
		consistent = (ToggleValue) values.get(2).getSecond();
		ignoreFriends = (ToggleValue) values.get(3).getSecond();
	}

}
