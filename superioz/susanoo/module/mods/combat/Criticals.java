package de.superioz.susanoo.module.mods.combat;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.event.events.MouseClickEvent;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import net.minecraft.block.material.Material;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.network.play.client.C03PacketPlayer;
import org.lwjgl.input.Keyboard;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 02.06.2016.
 */
public class Criticals extends Module {

	/**
	 * Constructor
	 */
	public Criticals(){
		super("criticals", Keyboard.KEY_NONE, ModuleCategory.COMBAT);
	}

	@Override
	public void onEnable(){
		//
	}

	@Override
	public void onDisable(){
		//
	}

	@Override
	public void onRender(float partialTicks){
		//
	}

	@Override
	public void onUpdate(){
		//
	}

	@Override
	public void onEvent(Event event){
		if(event instanceof MouseClickEvent){
			MouseClickEvent clickEvent = (MouseClickEvent) event;
			if(clickEvent.getType() != MouseClickEvent.Type.LEFTCLICK) return;

			if(mc.objectMouseOver != null
					&& mc.objectMouseOver.entityHit instanceof EntityLivingBase){
				doCritical();
			}
		}
	}

	public void doCritical(){
		if(!mc.thePlayer.isInWater()
				&& !mc.thePlayer.isInsideOfMaterial(Material.lava)
				&& mc.thePlayer.onGround){
			double posX = mc.thePlayer.posX;
			double posY = mc.thePlayer.posY;
			double posZ = mc.thePlayer.posZ;
			NetHandlerPlayClient sendQueue = mc.thePlayer.sendQueue;

			sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(posX,
					posY + 0.0625D, posZ, true));
			sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(posX,
					posY, posZ, false));
			sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(posX,
					posY + 1.1E-5D, posZ, false));
			sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(posX,
					posY, posZ, false));
		}
	}

}
