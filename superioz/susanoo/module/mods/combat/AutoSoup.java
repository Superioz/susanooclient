package de.superioz.susanoo.module.mods.combat;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.ConfigSaveableWrapper;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.module.spec.RangeValue;
import de.superioz.susanoo.module.spec.ToggleValue;
import de.superioz.susanoo.util.impl.SerializablePair;
import de.superioz.susanoo.util.module.MinecraftSouper;
import org.lwjgl.input.Keyboard;

import java.util.HashMap;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 24.06.2016.
 */
public class AutoSoup extends Module implements ConfigSaveable {

	public static RangeValue health = new RangeValue(1, 20, 11, 1);
	public static RangeValue soupDelay = new RangeValue(0, 100, 40, 1, "%val ms");
	public static ToggleValue refill = new ToggleValue(true);
	public static RangeValue refillDelay = new RangeValue(0, 100, 40, 1, "%val ms");

	private MinecraftSouper souper;

	/**
	 * Constructor
	 */
	public AutoSoup(){
		super("autosoup", Keyboard.KEY_NONE, ModuleCategory.COMBAT);
	}

	@Override
	public void onEnable(){
		souper = new MinecraftSouper(mc.thePlayer.inventory.currentItem);
	}

	@Override
	public void onDisable(){
		//
	}

	@Override
	public void onRender(float partialTicks){
		//
	}

	@Override
	public void onUpdate(){
		souper.update();
	}

	@Override
	public void onEvent(Event event){
		//
	}

	@Override
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues(){
		return ConfigSaveableWrapper.buildMap(
				new ConfigSaveableWrapper(health, "Health:", 0),
				new ConfigSaveableWrapper(soupDelay, "Soup Delay:", 1),
				new ConfigSaveableWrapper(refill, "Auto Refill:", 2),
				new ConfigSaveableWrapper(refillDelay, "Refill Delay:", 3)
		);
	}

	@Override
	public void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		health = (RangeValue) values.get(0).getSecond();
		soupDelay = (RangeValue) values.get(1).getSecond();
		refill = (ToggleValue) values.get(2).getSecond();
		refillDelay = (RangeValue) values.get(3).getSecond();
	}
}
