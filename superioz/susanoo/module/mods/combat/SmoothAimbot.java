package de.superioz.susanoo.module.mods.combat;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.ConfigSaveableWrapper;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.module.spec.RangeValue;
import de.superioz.susanoo.module.spec.ToggleValue;
import de.superioz.susanoo.util.module.MinecraftAimBot;
import de.superioz.susanoo.util.impl.SerializablePair;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MathHelper;
import org.lwjgl.input.Keyboard;

import java.util.HashMap;

/**
 * Created on 01.06.2016.
 */
public class SmoothAimbot extends Module implements ConfigSaveable {

	public static RangeValue range = new RangeValue(1, 10, 5, 1);
	public static RangeValue fov = new RangeValue(0, 360, 180, 90, "%val°");
	public static RangeValue speed = new RangeValue(1, 20, 5, 1);
	public static ToggleValue ignoreFriends = new ToggleValue(true);

	public static Entity target;
	public static Thread aimbot;

	/**
	 * Constructor
	 */
	public SmoothAimbot(){
		super("smoothaimbot", Keyboard.KEY_NONE, ModuleCategory.COMBAT);
	}

	@Override
	public void onEnable(){
		aimbot = new Thread(new MinecraftAimBot());
		aimbot.start();
	}

	@Override
	public void onDisable(){
		aimbot.stop();
		target = null;
	}

	@Override
	public void onRender(float partialTicks){
		//
	}

	@Override
	public void onUpdate(){
		//
	}

	@Override
	public void onEvent(Event event){
		//
	}

	/**
	 * This method doesn't face the player to the entity directly
	 * The rotation is smoother
	 * .synchronized cause this method will be called in a thread
	 *
	 * @param e The entity
	 */

	public static synchronized void faceEntity(Entity e){
		float[] rotations = getRotationsNeeded(e);
		float f0 = ((float) speed.getCurrent()) / 1000F;

		if(minecraft.thePlayer.rotationYaw < rotations[0] - 5.0F){
			minecraft.thePlayer.rotationYaw += f0;
		}
		else if(minecraft.thePlayer.rotationYaw > rotations[0] + 5.0F){
			minecraft.thePlayer.rotationYaw -= f0;
		}

		if(minecraft.thePlayer.rotationPitch < rotations[1] - 5.0F){
			minecraft.thePlayer.rotationPitch += f0;
		}
		else if(minecraft.thePlayer.rotationPitch > rotations[1] + 5.0F){
			minecraft.thePlayer.rotationPitch -= f0;
		}
	}

	/**
	 * Get all rotation the player need for facing the entity
	 *
	 * @param entity The entity
	 * @return The float array (yaw, pitch)
	 */
	public static float[] getRotationsNeeded(Entity entity){
		if(entity == null){
			return null;
		}

		double diffX = entity.posX - minecraft.thePlayer.posX;
		double diffZ = entity.posZ - minecraft.thePlayer.posZ;
		double diffY;

		if((entity instanceof EntityLivingBase)){
			EntityLivingBase entityLivingBase = (EntityLivingBase) entity;
			diffY = entityLivingBase.posY + entityLivingBase.getEyeHeight()
					- (minecraft.thePlayer.posY + minecraft.thePlayer.getEyeHeight())
					- Math.PI / 90.0F;
		}
		else{
			diffY = (entity.boundingBox.minY + entity.boundingBox.maxY) / 2.0D - (
					minecraft.thePlayer.posY + minecraft.thePlayer.getEyeHeight());
		}

		double dist = MathHelper.sqrt_double(diffX * diffX + diffZ * diffZ);
		float yaw = (float) (Math.atan2(diffZ, diffX) * 180.0D / Math.PI) - 90.0F;
		float pitch = (float) -(Math.atan2(diffY, dist) * 180.0D / Math.PI);
		return new float[]{minecraft.thePlayer.rotationYaw + MathHelper.wrapAngleTo180_float(yaw - minecraft.thePlayer.rotationYaw),
				minecraft.thePlayer.rotationPitch + MathHelper.wrapAngleTo180_float(pitch - minecraft.thePlayer.rotationPitch)
		};
	}

	/**
	 * Get the players from the mouse to the entity as rotation (FOV)
	 *
	 * @param entity The entity
	 * @return The players as angle
	 */
	public static int getDistanceFromMouse(Entity entity){
		float[] neededRotations = getRotationsNeeded(entity);

		if(neededRotations != null){
			float neededYaw = minecraft.thePlayer.rotationYaw - neededRotations[0];
			float neededPitch = minecraft.thePlayer.rotationPitch - neededRotations[1];
			float distanceFromMouse = MathHelper.sqrt_float(neededYaw * neededYaw + neededPitch * neededPitch);
			return (int) distanceFromMouse;
		}

		return -1;
	}

	@Override
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues(){
		return ConfigSaveableWrapper.buildMap(
				new ConfigSaveableWrapper(range, "Range:", 0),
				new ConfigSaveableWrapper(fov, "FOV:", 1),
				new ConfigSaveableWrapper(speed, "Speed:", 2),
				new ConfigSaveableWrapper(ignoreFriends, "Ignore Friends:", 3)
		);
	}

	@Override
	public void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		if(values == null){
			return;
		}

		range = (RangeValue) values.get(0).getSecond();
		fov = (RangeValue) values.get(1).getSecond();
		speed = (RangeValue) values.get(2).getSecond();
		ignoreFriends = (ToggleValue) values.get(3).getSecond();
	}
}
