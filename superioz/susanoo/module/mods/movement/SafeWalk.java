package de.superioz.susanoo.module.mods.movement;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.event.events.SafeWalkEvent;
import de.superioz.susanoo.module.ConfigSaveableWrapper;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.module.spec.ToggleValue;
import de.superioz.susanoo.util.impl.SerializablePair;
import org.lwjgl.input.Keyboard;

import java.util.HashMap;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 25.06.2016.
 */
public class SafeWalk extends Module implements ConfigSaveable {

	public static ToggleValue onJumping = new ToggleValue(false);

	/**
	 * Constructor
	 */
	public SafeWalk(){
		super("safewalk", Keyboard.KEY_NONE, ModuleCategory.MOVEMENT);
	}

	@Override
	public void onEnable(){

	}

	@Override
	public void onDisable(){

	}

	@Override
	public void onRender(float partialTicks){

	}

	@Override
	public void onUpdate(){

	}

	@Override
	public void onEvent(Event event){
		if(event instanceof SafeWalkEvent){
			if(mc.thePlayer.onGround || onJumping.getState())
				((SafeWalkEvent) event).shouldWalkSafe = true;
		}
	}

	@Override
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues(){
		return ConfigSaveableWrapper.buildMap(
				new ConfigSaveableWrapper(onJumping, "While Jumping:", 0)
		);
	}

	@Override
	public void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		onJumping = (ToggleValue) values.get(0).getSecond();
	}
}
