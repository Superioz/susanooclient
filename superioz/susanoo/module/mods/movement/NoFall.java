package de.superioz.susanoo.module.mods.movement;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import net.minecraft.network.play.client.C03PacketPlayer;
import org.lwjgl.input.Keyboard;

/**
 * Created on 25.05.2016.
 */
public class NoFall extends Module {

	public NoFall(){
		super("nofall", Keyboard.KEY_NONE, ModuleCategory.MOVEMENT);
	}

	@Override
	public void onEnable(){
		//
	}

	@Override
	public void onDisable(){
		//
	}

	@Override
	public void onRender(float partialTicks){
		//
	}

	@Override
	public void onUpdate(){
		if(minecraft.thePlayer.fallDistance > 2F){
			minecraft.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer(true));
		}
	}

	@Override
	public void onEvent(Event event){
		//
	}
}
