package de.superioz.susanoo.module.mods.movement;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import org.lwjgl.input.Keyboard;

/**
 * Created on 01.06.2016.
 */
public class Spider extends Module
{
    private static final double motionY = 0.2;

    /**
     * Constructor
     */
    public Spider()
    {
        super("spider", Keyboard.KEY_NONE, ModuleCategory.MOVEMENT);
    }

    @Override
    public void onEnable()
    {
    }

    @Override
    public void onDisable()
    {
    }

    @Override
    public void onRender(float partialTicks)
    {
    }

    @Override
    public void onUpdate()
    {
        if (minecraft.thePlayer.isCollidedHorizontally)
        {
            minecraft.thePlayer.motionY = motionY;
        }
    }

    @Override
    public void onEvent(Event event)
    {
    }
}
