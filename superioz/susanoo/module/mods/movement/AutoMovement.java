package de.superioz.susanoo.module.mods.movement;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.ConfigSaveableWrapper;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.module.spec.ToggleValue;
import de.superioz.susanoo.util.impl.SerializablePair;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Keyboard;

import java.util.HashMap;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 24.06.2016.
 */
public class AutoMovement extends Module implements ConfigSaveable {

	private ToggleValue jump = new ToggleValue(false);
	private ToggleValue sprint = new ToggleValue(false);
	private ToggleValue step = new ToggleValue(false);
	private ToggleValue swim = new ToggleValue(false);
	private ToggleValue walk = new ToggleValue(false);

	/**
	 * Constructor
	 */
	public AutoMovement(){
		super("automovement", Keyboard.KEY_NONE, ModuleCategory.MOVEMENT);
	}

	@Override
	public void onEnable(){

	}

	@Override
	public void onDisable(){
		if(step.getState()){
			minecraft.thePlayer.stepHeight = 0.5F;
		}
	}

	@Override
	public void onRender(float partialTicks){

	}

	@Override
	public void onUpdate(){
		if(jump.getState()){
			if((mc.thePlayer.moveForward != 0 || Minecraft.getMinecraft().thePlayer.moveStrafing != 0)
					&& !mc.thePlayer.isSneaking() && mc.thePlayer.onGround){
				mc.thePlayer.jump();
			}
		}
		if(sprint.getState()){
			if((!minecraft.thePlayer.isOnLadder()) && (!minecraft.thePlayer.isCollidedHorizontally) && (!minecraft.thePlayer.isSneaking()) &&
					(!minecraft.thePlayer.isUsingItem()) && (minecraft.thePlayer.moveForward > 0.0F) &&
					(minecraft.thePlayer.getFoodStats().getFoodLevel() > 6)){
				minecraft.thePlayer.setSprinting(true);
			}
		}
		if(step.getState()){
			minecraft.thePlayer.stepHeight = 1F;
		}
		if(swim.getState()){
			if(mc.thePlayer.handleWaterMovement()){
				mc.thePlayer.setSprinting(true);
				mc.thePlayer.motionY += 0.04;
				mc.thePlayer.motionX = mc.thePlayer.motionX*1.02;
				mc.thePlayer.motionZ = mc.thePlayer.motionZ*1.02;
			}
		}
		if(walk.getState()){
			if(!minecraft.gameSettings.keyBindForward.pressed){
				minecraft.gameSettings.keyBindForward.pressed = true;
			}
		}
	}

	@Override
	public void onEvent(Event event){

	}

	@Override
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues(){
		return ConfigSaveableWrapper.buildMap(
				new ConfigSaveableWrapper(jump, "Jump:", 0),
				new ConfigSaveableWrapper(sprint, "Sprint:", 1),
				new ConfigSaveableWrapper(step, "Step:", 2),
				new ConfigSaveableWrapper(swim, "Swim:", 3),
				new ConfigSaveableWrapper(walk, "Walk:", 4)
		);
	}

	@Override
	public void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		if(values == null){
			return;
		}

		jump = (ToggleValue) values.get(0).getSecond();
		sprint = (ToggleValue) values.get(1).getSecond();
		step = (ToggleValue) values.get(2).getSecond();
		swim = (ToggleValue) values.get(3).getSecond();
		walk = (ToggleValue) values.get(4).getSecond();
	}
}
