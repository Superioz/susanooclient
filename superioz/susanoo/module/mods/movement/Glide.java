package de.superioz.susanoo.module.mods.movement;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import net.minecraft.block.material.Material;
import org.lwjgl.input.Keyboard;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 22.06.2016.
 */
public class Glide extends Module {

	/**
	 * Constructor
	 */
	public Glide(){
		super("glide", Keyboard.KEY_NONE, ModuleCategory.MOVEMENT);
	}

	@Override
	public void onEnable(){

	}

	@Override
	public void onDisable(){

	}

	@Override
	public void onRender(float partialTicks){

	}

	@Override
	public void onUpdate(){
		if(mc.thePlayer.motionY < 0 && mc.thePlayer.isAirBorne
				&& !mc.thePlayer.isInWater() && !mc.thePlayer.isOnLadder()
				&& !mc.thePlayer.isInsideOfMaterial(Material.lava)){
			mc.thePlayer.motionY = -0.125f;
			mc.thePlayer.jumpMovementFactor *= 1.21337f;
		}
	}

	@Override
	public void onEvent(Event event){

	}
}
