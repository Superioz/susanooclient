package de.superioz.susanoo.module.mods.movement;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.input.Keyboard;

/**
 * Created on 25.05.2016.
 */
public class Flight extends Module
{
    public float speed = 1.5F;
    public double flyHeight;
    private double startY;

    public Flight()
    {
        super("flight", Keyboard.KEY_NONE, ModuleCategory.MOVEMENT);
    }

    @Override
    public void onEnable()
    {
        double startX = minecraft.thePlayer.posX;
        this.startY = minecraft.thePlayer.posY;
        double startZ = minecraft.thePlayer.posZ;

        for (int i = 0; i < 4; i++)
        {
            minecraft.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(
                        startX, this.startY + 1.01D, startZ, false));
            minecraft.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(
                        startX, this.startY, startZ, false));
        }

        minecraft.thePlayer.jump();
    }

    @Override
    public void onDisable()
    {
        minecraft.thePlayer.capabilities.isFlying = false;
    }

    @Override
    public void onRender(float partialTicks)
    {
        //
    }

    @Override
    public void onUpdate()
    {
        if (!minecraft.thePlayer.onGround)
        {
            if ((minecraft.gameSettings.keyBindJump.pressed) && (minecraft.thePlayer.posY < this.startY - 1.0D))
            {
                minecraft.thePlayer.motionY = 0.2D;
            }
            else
            {
                minecraft.thePlayer.motionY = -0.02D;
            }
        }
    }

    @Override
    public void onEvent(Event event)
    {
        //
    }

    public void updateFlyHeight()
    {
        double h = 1.0D;
        AxisAlignedBB box =
            minecraft.thePlayer.getEntityBoundingBox().expand(0.0625D, 0.0625D, 0.0625D);

        for (this.flyHeight = 0.0D; this.flyHeight < minecraft.thePlayer.posY; this.flyHeight += h)
        {
            AxisAlignedBB nextBox = box.offset(0.0D, -this.flyHeight, 0.0D);

            if (minecraft.theWorld.checkBlockCollision(nextBox))
            {
                if (h < 0.0625D)
                {
                    break;
                }

                this.flyHeight -= h;
                h /= 2.0D;
            }
        }
    }

    public void goToGround()
    {
        if (this.flyHeight > 300.0D)
        {
            return;
        }

        double minY = minecraft.thePlayer.posY - this.flyHeight;

        if (minY <= 0.0D)
        {
            return;
        }

        for (double y = minecraft.thePlayer.posY; y > minY;)
        {
            y -= 8.0D;

            if (y < minY)
            {
                y = minY;
            }

            C03PacketPlayer.C04PacketPlayerPosition packet = new C03PacketPlayer.C04PacketPlayerPosition(
                minecraft.thePlayer.posX, y, minecraft.thePlayer.posZ, true);
            minecraft.thePlayer.sendQueue.addToSendQueue(packet);
        }

        for (double y = minY; y < minecraft.thePlayer.posY;)
        {
            y += 8.0D;

            if (y > minecraft.thePlayer.posY)
            {
                y = minecraft.thePlayer.posY;
            }

            C03PacketPlayer.C04PacketPlayerPosition packet =
                new C03PacketPlayer.C04PacketPlayerPosition(
                minecraft.thePlayer.posX, y, minecraft.thePlayer.posZ, true);
            minecraft.thePlayer.sendQueue.addToSendQueue(packet);
        }
    }
}
