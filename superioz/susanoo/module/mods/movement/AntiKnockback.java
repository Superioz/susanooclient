package de.superioz.susanoo.module.mods.movement;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.event.events.EntityVelocityEvent;
import de.superioz.susanoo.module.ConfigSaveableWrapper;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.module.spec.RangeValue;
import de.superioz.susanoo.util.impl.SerializablePair;
import org.lwjgl.input.Keyboard;

import java.util.HashMap;

/**
 * Created on 24.06.2016.
 */
public class AntiKnockback extends Module implements ConfigSaveable {

	private RangeValue strenghtX = new RangeValue(1, 100, 1, 1, "%val%");
	private RangeValue strenghtY = new RangeValue(1, 100, 1, 1, "%val%");
	private RangeValue strenghtZ = new RangeValue(1, 100, 1, 1, "%val%");

	/**
	 * Constructor
	 */
	public AntiKnockback(){
		super("antiknockback", Keyboard.KEY_NONE, ModuleCategory.MOVEMENT);
	}

	@Override
	public void onEnable(){

	}

	@Override
	public void onDisable(){

	}

	@Override
	public void onRender(float partialTicks){

	}

	@Override
	public void onUpdate(){

	}

	@Override
	public void onEvent(Event event){
		if(event instanceof EntityVelocityEvent){
			EntityVelocityEvent velocityEvent = (EntityVelocityEvent) event;
			velocityEvent.setX(velocityEvent.getX()/100 * strenghtX.getCurrent());
			velocityEvent.setY(velocityEvent.getY()/100 * strenghtY.getCurrent());
			velocityEvent.setZ(velocityEvent.getZ()/100 * strenghtZ.getCurrent());
		}
	}

	@Override
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues(){
		return ConfigSaveableWrapper.buildMap(
				new ConfigSaveableWrapper(strenghtX, "Knockback X:", 0),
				new ConfigSaveableWrapper(strenghtY, "Knockback Y:", 1),
				new ConfigSaveableWrapper(strenghtZ, "Knockback Z:", 2)
		);
	}

	@Override
	public void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		strenghtX = (RangeValue) values.get(0).getSecond();
		strenghtY = (RangeValue) values.get(1).getSecond();
		strenghtZ = (RangeValue) values.get(2).getSecond();
	}
}
