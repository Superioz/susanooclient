package de.superioz.susanoo.module.mods.movement;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import org.lwjgl.input.Keyboard;

/**
 * Created on 22.06.2016.
 * Code by @Wurst_Client
 */
public class Speed extends Module {

	/**
	 * Constructor
	 */
	public Speed(){
		super("speed", Keyboard.KEY_NONE, ModuleCategory.MOVEMENT);
	}

	@Override
	public void onEnable(){

	}

	@Override
	public void onDisable(){

	}

	@Override
	public void onRender(float partialTicks){

	}

	@Override
	public void onUpdate(){

	}

	@Override
	public void onEvent(Event event){

	}


}
