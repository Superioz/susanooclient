package de.superioz.susanoo.module.mods.world;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import org.lwjgl.input.Keyboard;

/**
 * Created on 01.06.2016.
 */
public class FastPlace extends Module
{
    /**
     * Constructor
     *
     */
    public FastPlace()
    {
        super("fastplace", Keyboard.KEY_NONE, ModuleCategory.WORLD);
    }

    @Override
    public void onEnable()
    {
    }

    @Override
    public void onDisable()
    {
    }

    @Override
    public void onRender(float partialTicks)
    {
    }

    @Override
    public void onUpdate()
    {
        minecraft.rightClickDelayTimer = 0;
    }

    @Override
    public void onEvent(Event event)
    {
    }
}
