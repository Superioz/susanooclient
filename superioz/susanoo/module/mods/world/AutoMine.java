package de.superioz.susanoo.module.mods.world;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import net.minecraft.block.Block;
import org.lwjgl.input.Keyboard;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 25.06.2016.
 */
public class AutoMine extends Module {

	/**
	 * Constructor
	 */
	public AutoMine(){
		super("automine", Keyboard.KEY_NONE, ModuleCategory.WORLD);
	}

	@Override
	public void onEnable(){
		mc.gameSettings.keyBindAttack.pressed = false;
	}

	@Override
	public void onDisable(){
		mc.gameSettings.keyBindAttack.pressed = false;
	}

	@Override
	public void onRender(float partialTicks){

	}

	@Override
	public void onUpdate(){
		if(mc.objectMouseOver == null || mc.objectMouseOver.getBlockPos() == null){
			return;
		}
		mc.gameSettings.keyBindAttack.pressed =
				Block.getIdFromBlock(mc.theWorld.getBlockState(mc.objectMouseOver.getBlockPos()).getBlock()) != 0;
	}

	@Override
	public void onEvent(Event event){

	}
}
