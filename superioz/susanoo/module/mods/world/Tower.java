package de.superioz.susanoo.module.mods.world;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.ConfigSaveableWrapper;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.module.spec.ListValue;
import de.superioz.susanoo.module.spec.RangeValue;
import de.superioz.susanoo.util.Timer;
import de.superioz.susanoo.util.impl.SerializablePair;
import net.minecraft.block.BlockAir;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Vec3;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 25.06.2016.
 */
public class Tower extends Module implements ConfigSaveable {

	private RangeValue delay = new RangeValue(0, 250, 50, 1, "%val ms");
	private ListValue mode = new ListValue(Arrays.asList("Legit", "Unlegit"), 0);

	private Timer timer = new Timer();
	private EntityPlayerSP p;

	/**
	 * Constructor
	 */
	public Tower(){
		super("tower", Keyboard.KEY_NONE, ModuleCategory.WORLD);
	}

	@Override
	public void onEnable(){
		p = mc.thePlayer;
		timer.reset();
	}

	@Override
	public void onDisable(){

	}

	@Override
	public void onRender(float partialTicks){

	}

	@Override
	public void onUpdate(){
		ItemStack current = p.getHeldItem();
		if(current == null
				|| !(current.getItem() instanceof ItemBlock)){
			return;
		}

		if(mode.getCurrentIndex() == 0){
			if(p.onGround)
				p.jump();

			if(timer.hasReached(delay.getCurrent())){
				BlockPos pos = new BlockPos((int) Math.floor(p.posX), p.posY - 1, (int) Math.floor(p.posZ));
				if(!canPlaceBlock(pos)) return;

				mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C05PacketPlayerLook(0.0F, 90.0F, true));
				mc.playerController.onPlayerRightClick(
						mc.thePlayer, mc.theWorld, mc.thePlayer.inventory.getCurrentItem(),
						pos.add(0, -1, 0), EnumFacing.UP, new Vec3(pos.getX(), pos.getY(), pos.getZ()));
				mc.thePlayer.swingItem();
			}
		}
		else{
			if(!timer.hasReached(delay.getCurrent())){
				return;
			}
			p.jump();

			for(BlockPos pos : getCollisionBlocks()){
				IBlockState state = p.worldObj.getBlockState(pos);

				// Can a block be placed?
				if(state.getBlock() instanceof BlockAir){
					mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C05PacketPlayerLook(0.0F, 90.0F, true));
					mc.playerController.onPlayerRightClick(
							mc.thePlayer, mc.theWorld, mc.thePlayer.inventory.getCurrentItem(),
							pos.add(0, -1, 0), EnumFacing.UP, new Vec3(pos.getX(), pos.getY(), pos.getZ()));
					mc.thePlayer.swingItem();

					//
					p.motionY = 0.0D;
				}
			}
		}
	}

	@Override
	public void onEvent(Event event){

	}

	private boolean canPlaceBlock(BlockPos pos){
		IBlockState state = mc.theWorld.getBlockState(pos);
		IBlockState below = mc.theWorld.getBlockState(pos.down());

		if(!below.getBlock().getMaterial().isSolid()){
			return false;
		}
		return true;
	}

	/**
	 * Get all blocks player can collide with
	 *
	 * @return The list of positions
	 */
	private ArrayList<BlockPos> getCollisionBlocks(){
		ArrayList<BlockPos> collidedBlocks = new ArrayList<>();

		for(int x = (int) Math.floor(p.posX); x <= (int) Math.floor(p.posX); x++){
			for(int y = (int) p.posY - 1; y <= (int) p.posY; y++){
				for(int z = (int) Math.floor(p.posZ); z <= (int) Math.floor(p.posZ); z++){
					BlockPos blockPos = new BlockPos(x, y, z);

					try{
						if(y > p.posY - 2.0D && y <= p.posY - 1.0D){
							collidedBlocks.add(blockPos);
						}
					}
					catch(Exception e){
						// Minecraft is shit.
					}
				}
			}
		}
		return collidedBlocks;
	}

	@Override
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues(){
		return ConfigSaveableWrapper.buildMap(
				new ConfigSaveableWrapper(delay, "Delay:", 0),
				new ConfigSaveableWrapper(mode, "Mode:", 1)
		);
	}

	@Override
	public void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		delay = (RangeValue) values.get(0).getSecond();
		mode = (ListValue) values.get(1).getSecond();
	}
}
