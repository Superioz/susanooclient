package de.superioz.susanoo.module.mods.world;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.ConfigSaveableWrapper;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.module.spec.RangeValue;
import de.superioz.susanoo.module.spec.SingleplayerBased;
import de.superioz.susanoo.util.Colors;
import de.superioz.susanoo.util.render.RenderUtil;
import de.superioz.susanoo.util.impl.SerializablePair;
import net.minecraft.block.material.Material;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created on 25.05.2016.
 */
public class Nuker extends Module implements SingleplayerBased, ConfigSaveable {

	private RangeValue radius = new RangeValue(3, 15, 3, 3);

	public Nuker(){
		super("nuker", Keyboard.KEY_NONE, ModuleCategory.WORLD);
	}

	@Override
	public void onEnable(){
		//
	}

	@Override
	public void onDisable(){
		//
	}

	@Override
	public void onRender(float partialTicks){
		for(BlockPos pos : getBlocksInRadius()){
			double renderX = pos.getX() - minecraft.getRenderManager().renderPosX;
			double renderY = pos.getY() - minecraft.getRenderManager().renderPosY;
			double renderZ = pos.getZ() - minecraft.getRenderManager().renderPosZ;
			AxisAlignedBB bb = new AxisAlignedBB(renderX, renderY, renderZ, renderX + 1d, renderY + 1d, renderZ + 1d);
			RenderUtil.outline(bb, Colors.WHITE, 1.5f);
		}
	}

	@Override
	public void onUpdate(){
		for(BlockPos pos : getBlocksInRadius()){
			minecraft.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.START_DESTROY_BLOCK,
					pos, EnumFacing.SOUTH));
			minecraft.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.STOP_DESTROY_BLOCK,
					pos, EnumFacing.SOUTH));
		}
	}

	@Override
	public void onEvent(Event event){
		//
	}

	/**
	 * Get all blocks within static radius from this class
	 *
	 * @return The list of positions
	 */
	public List<BlockPos> getBlocksInRadius(){
		List<BlockPos> positions = new ArrayList<BlockPos>();
		int r = radius.getCurrent();

		for(int x = -r; x <= r; x++){
			for(int y = -r; y <= r; y++){
				for(int z = -r; z <= r; z++){
					if((x * x + y * y + z * z) > (r * r)){
						continue;
					}

					int posX = (int) minecraft.thePlayer.posX + x;
					int posY = (int) minecraft.thePlayer.posY + y;
					int posZ = (int) minecraft.thePlayer.posZ + z;
					BlockPos pos = new BlockPos(posX, posY, posZ);

					if(posY > 127 || posY < 0){
						continue;
					}

					if(minecraft.theWorld.getBlockState(pos).getBlock().getMaterial() == Material.air){
						continue;
					}

					positions.add(pos);
				}
			}
		}

		return positions;
	}

	@Override
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues(){
		return ConfigSaveableWrapper.buildMap(
				new ConfigSaveableWrapper(radius, "Radius:", 0)
		);
	}

	@Override
	public void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		if(values == null){
			return;
		}

		radius = (RangeValue) values.get(0).getSecond();
	}
}
