package de.superioz.susanoo.module.mods.world;

import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import net.minecraft.entity.projectile.EntityFishHook;
import org.lwjgl.input.Keyboard;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 25.06.2016.
 */
public class AutoFish extends Module {

	private boolean catching = false;

	/**
	 * Constructor
	 */
	public AutoFish(){
		super("autofish", Keyboard.KEY_NONE, ModuleCategory.WORLD);
	}

	@Override
	public void onEnable(){

	}

	@Override
	public void onDisable(){

	}

	@Override
	public void onRender(float partialTicks){

	}

	@Override
	public void onUpdate(){
		if(mc.thePlayer.fishEntity != null && isHooked(mc.thePlayer.fishEntity)
				&& !catching){
			catching = true;
			mc.rightClickMouse();
			new Thread("AutoFish") {
				@Override
				public void run(){
					try{
						Thread.sleep(1000);
					}
					catch(InterruptedException e){
						e.printStackTrace();
					}
					mc.rightClickMouse();
					catching = false;
				}
			}.start();
		}
	}

	@Override
	public void onEvent(Event event){

	}

	private boolean isHooked(EntityFishHook hook){
		return hook.motionX == 0.0D && hook.motionZ == 0.0D
				&& hook.motionY != 0.0D;
	}

}
