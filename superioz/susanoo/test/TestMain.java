package de.superioz.susanoo.test;

/**
 * Created on 22.06.2016.
 */
public class TestMain {

	public static void main(String[] args){
		int diff = 90;

		for(int i = 0; i <= 180; i++){
			int modulo = i % diff;

			if(diff-modulo <= 5 || modulo <= 5){
				System.out.println(i + " % " + diff + " = " + (modulo));
			}
		}
	}

}
