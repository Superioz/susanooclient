package de.superioz.susanoo.cosmetic;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 01.06.2016.
 */
public class CapeManager {

	public static HashMap<String, String> CAPE_MAP = new HashMap<String, String>();

	public static void init(){
		CAPE_MAP.put("Assex_", Capes.REALMS_CREATOR.getUrl());
		CAPE_MAP.put("StalkerEdga", Capes.REALMS_CREATOR.getUrl());
//        CAPE_MAP.put("Klopser", Capes.MINECON_2013.getUrl());
//        CAPE_MAP.put("Superioz", Capes.COBALT.getUrl());
	}

	public static void getCapes(GameProfile gameProfile,
	                            Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> skinMap){
		if(CAPE_MAP.containsKey(gameProfile.getName())){
			skinMap.put(MinecraftProfileTexture.Type.CAPE, new MinecraftProfileTexture(CAPE_MAP.get(gameProfile.getName()), null));
		}
	}

	public enum Capes {
		MOJANG_CAPE_OLD("https://hydra-media.cursecdn.com/minecraft.gamepedia.com/8/8c/Mojang_cape.png"),
		MOJANG_CAPE_NEW("https://hydra-media.cursecdn.com/minecraft.gamepedia.com/5/57/MojangCape2016.png"),
		XMAS_2010("https://hydra-media.cursecdn.com/minecraft.gamepedia.com/archive/3/33/20110129073421!Xmas.png"),
		NEW_YEAR("https://hydra-media.cursecdn.com/minecraft.gamepedia.com/archive/b/b4/20110129073427!2011.png"),
		MINECON_2011("https://hydra-media.cursecdn.com/minecraft.gamepedia.com/7/76/MineCon_Cape_2011.png"),
		MINECON_2012("https://hydra-media.cursecdn.com/minecraft.gamepedia.com/9/9a/Minecon_Cape2012.png"),
		MINECON_2013("https://hydra-media.cursecdn.com/minecraft.gamepedia.com/1/1b/Official_MineCon_2013_Cape.png"),
		MINECON_2015("https://hydra-media.cursecdn.com/minecraft.gamepedia.com/2/21/Minecon2015CapeTexture.png"),
		MINECON_2016_PRE("http://www.minecraftcapes.co.uk/gallery/CapePictures/Minecon-capes/capeFile/2016.png"),
		REALMS_CREATOR("https://hydra-media.cursecdn.com/minecraft.gamepedia.com/c/c3/MapmakerCapeTexture.png?version=a0ebd06cf1d35ff92ca01fe720f62a77"),
		COBALT("https://hydra-media.cursecdn.com/minecraft.gamepedia.com/8/8e/Cobalt_Cape.png"),;

		String url;

		Capes(String s){
			url = s;
		}

		public String getUrl(){
			return url;
		}
	}
}
