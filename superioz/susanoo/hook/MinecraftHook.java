package de.superioz.susanoo.hook;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import de.superioz.susanoo.Client;
import de.superioz.susanoo.cosmetic.CapeManager;
import de.superioz.susanoo.event.EventManager;
import de.superioz.susanoo.event.events.*;
import de.superioz.susanoo.module.ModuleManager;
import de.superioz.susanoo.module.mods.render.ChestESP;
import de.superioz.susanoo.module.mods.render.PlayerESP;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.gui.GuiIngame;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.Packet;
import net.minecraft.src.BlockPosM;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MovingObjectPosition;

import java.util.Map;

/**
 * Created on 21.06.2016.
 */
public class MinecraftHook {

	public static void onStartup(){
		new Client();
		EventManager.callEvent(new ClientStartupEvent());
	}

	public static void onShutdown(){
		EventManager.callEvent(new ClientShutdownEvent());
	}

	public static void onMouseMiddleClick(){
		EventManager.callEvent(new MouseClickEvent(MouseClickEvent.Type.MIDDLECLICK));
	}

	public static void onMouseLeftClick(){
		EventManager.callEvent(new MouseClickEvent(MouseClickEvent.Type.LEFTCLICK));
	}

	public static void onPlayerPreAttack(EntityPlayer thePlayer, MovingObjectPosition objectMouseOver){
		EventManager.callEvent(new PlayerPreAttackEvent(thePlayer, objectMouseOver.entityHit));
	}

	public static void onMouseRightClick(){
		EventManager.callEvent(new MouseClickEvent(MouseClickEvent.Type.RIGHTCLICK));
	}

	public static void onKeyPress(int key){
		EventManager.callEvent(new KeyPressEvent(key));
	}

	public static void onOverlayRender(GuiIngame ingame){
		EventManager.callEvent(new ClientOverlayRenderEvent(ingame));
	}

	public static void onSuperSecretSettings(){
		if(Client.gui == null || !Client.gui.isVisible()){
			Client.gui.setVisible(true);
		}
	}

	public static String onRenderString(String s){
		RenderStringEvent event = new RenderStringEvent(s);
		EventManager.callEvent(event);
		return event.getString();
	}

	public static boolean onChatMessage(String message){
		SendMessageEvent event = new SendMessageEvent(message);
		EventManager.callEvent(event);

		return !event.isCancelled();
	}

	public static void onClientUpdate(){
		EventManager.callEvent(new ClientUpdateEvent());
	}

	public static void onPlayerSlowdown(){
		PlayerSlowdownEvent event = new PlayerSlowdownEvent();
		EventManager.callEvent(event);
	}

	public static EntityVelocityEvent onPlayerVelocity(Entity entity, double vX, double vY, double vZ){
		EntityVelocityEvent event = new EntityVelocityEvent(entity, vX, vY, vZ);
		EventManager.callEvent(event);
		return event;
	}

	public static void onPostRender3D(){
		EventManager.callEvent(new PostRender3DEvent());
	}

	public static void onClientRender(float partialTicks){
		EventManager.callEvent(new ClientRenderEvent(partialTicks));
	}

	public static void onPostAttack(EntityPlayer playerIn, Entity target){
		EventManager.callEvent(new PlayerPostAttackEvent(playerIn, target));
	}

	public static Packet onPacketReceive(Packet packet){
		PacketReceiveEvent event = new PacketReceiveEvent(packet);
		EventManager.callEvent(event);

		if(event.isCancelled()) return null;
		return event.getPacket();
	}

	public static Packet onPacketSend(Packet packet){
		PacketSendEvent sendEvent = new PacketSendEvent(packet);
		EventManager.callEvent(sendEvent);

		return sendEvent.getPacket();
	}

	public static void onCapeRender(GameProfile profile, Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> skinMap){
		if(Client.options.susanooCapes.getState()){
			CapeManager.getCapes(profile, skinMap);
		}
	}

	public static void onBlockRender(BlockPosM blockPosM, Block blockIn){
		if(ModuleManager.getModule(ChestESP.class).isActive()){
			BlockPos p = new BlockPos(
					blockPosM.getX(),
					blockPosM.getY(),
					blockPosM.getZ()
			);
			boolean b0 = ChestESP.BLOCK_ESP.hasBlock(p);
			boolean b1 = ChestESP.BLOCK_ESP.getPositions().contains(p);

			if(blockIn.getMaterial() != Material.air && (b0 || b1)){
				ChestESP.BLOCK_ESP.onUpdate(p);
			}
		}
	}

	public static boolean onPlayerTagRender(Entity entityIn){
		PlayerESP playerESP = (PlayerESP) ModuleManager.getModule(PlayerESP.class);

		if(playerESP.isActive()
				&& playerESP.nametag.getState()
				&& entityIn instanceof EntityPlayer){
			return false;
		}
		return true;
	}

	public static void onPlayerPreMotion(){
		EventManager.callEvent(new PreMotionEvent());
	}

	public static void onPlayerPostMotion(){
		EventManager.callEvent(new PostMotionEvent());
	}

	public static PlayerMotionEvent onPlayerMotion(double x, double y, double z){
		PlayerMotionEvent event = new PlayerMotionEvent(x, y, z);
		EventManager.callEvent(event);
		return event;
	}

}
