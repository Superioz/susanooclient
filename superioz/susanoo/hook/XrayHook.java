package de.superioz.susanoo.hook;

import de.superioz.susanoo.module.ModuleManager;
import de.superioz.susanoo.module.mods.render.Xray;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.BlockModelRenderer;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.IBlockAccess;

import java.nio.ByteOrder;

/**
 * Created on 21.06.2016.
 */
public class XrayHook {

	private static final int OPACITY = 200;

	/**
	 * Is Active
	 *
	 * @return The result
	 */
	public static boolean isActive(){
		return get().isActive();
	}

	/**
	 * Get the xray object
	 *
	 * @return The xray
	 */
	public static Xray get(){
		return (Xray) ModuleManager.getModule(Xray.class);
	}

	/**
	 * Should side be rendered override
	 *
	 * @param worldIn The world
	 * @param pos     The pos
	 * @param side    The side
	 * @param block   The block
	 * @return The result
	 */
	public static boolean shouldSideBeRendered(IBlockAccess worldIn, BlockPos pos, EnumFacing side, Block block){
		if(isActive()){
			if(get().opacityMode.getState()){
				if(Xray.isXrayBlock(Block.getIdFromBlock(block))){
					return true;
				}
			}
			else{
				return Xray.isXrayBlock(Block.getIdFromBlock(block));
			}
		}
		return side == EnumFacing.DOWN
				&& block.minY > 0.0D || (side == EnumFacing.UP
				&& block.maxY < 1.0D || (side == EnumFacing.NORTH
				&& block.minZ > 0.0D || (side == EnumFacing.SOUTH
				&& block.maxZ < 1.0D || (side == EnumFacing.WEST
				&& block.minX > 0.0D || (side == EnumFacing.EAST
				&& block.maxX < 1.0D || !worldIn.getBlockState(pos).getBlock().isOpaqueCube())))));

	}

	/**
	 * Renders a block model
	 *
	 * @param worldRendererIn The worldRenderer
	 * @param blockIn         The block
	 * @param aoFaceIn        The block face
	 */
	public static void renderBlockModel1(WorldRenderer worldRendererIn, Block blockIn,
	                                     BlockModelRenderer.AmbientOcclusionFace aoFaceIn){
		for(int i = 0; i < 4; i++){
			if(ModuleManager.getModule(Xray.class).isActive()){
				putXrayColorMultiplier(worldRendererIn, blockIn, aoFaceIn.vertexColorMultiplier[i],
						aoFaceIn.vertexColorMultiplier[i], aoFaceIn.vertexColorMultiplier[i], OPACITY, 4 - i);
			}
			else{
				worldRendererIn.putColorMultiplier(aoFaceIn.vertexColorMultiplier[i],
						aoFaceIn.vertexColorMultiplier[i], aoFaceIn.vertexColorMultiplier[i], 4 - i);
			}
		}
	}

	/**
	 * Renders a block model
	 *
	 * @param worldRendererIn The worldRenderer
	 * @param blockIn         The block
	 * @param aoFaceIn        The block face
	 */
	public static void renderBlockModel2(WorldRenderer worldRendererIn, Block blockIn,
	                                     BlockModelRenderer.AmbientOcclusionFace aoFaceIn,
	                                     float mod1, float mod2, float mod3){
		for(int i = 0; i < 4; i++){
			if(ModuleManager.getModule(Xray.class).isActive()){
				int opacity = 200;
				putXrayColorMultiplier(worldRendererIn, blockIn, aoFaceIn.vertexColorMultiplier[i] * mod1,
						aoFaceIn.vertexColorMultiplier[i] * mod2, aoFaceIn.vertexColorMultiplier[i] * mod3, opacity, 4 - i);
			}
			else{
				worldRendererIn.putColorMultiplier(aoFaceIn.vertexColorMultiplier[i] * mod1,
						aoFaceIn.vertexColorMultiplier[i] * mod2, aoFaceIn.vertexColorMultiplier[i] * mod3, 4 - i);
			}
		}
	}

	/**
	 * Put color multiplier for block with alpha value
	 *
	 * @param renderer    The renderer
	 * @param block       The block
	 * @param red         Red
	 * @param green       Green
	 * @param blue        Blue
	 * @param alpha       Alpha
	 * @param p_178978_4_ ?
	 */
	public static void putXrayColorMultiplier(WorldRenderer renderer, Block block, float red, float green, float blue, float alpha,
	                                          int p_178978_4_){
		int i = renderer.getColorIndex(p_178978_4_);
		int j = -1;

		if(!renderer.needsUpdate){
			j = renderer.rawIntBuffer.get(i);

			// Client
			if(ByteOrder.nativeOrder() == ByteOrder.LITTLE_ENDIAN){
				int k = (int) ((float) (j & 255) * red);
				int l = (int) ((float) (j >> 8 & 255) * green);
				int i1 = (int) ((float) (j >> 16 & 255) * blue);
				int a1 = (int) ((float) (j >> 24 & 255) * alpha);
				//j = j & -16777216;
				j = a1 << 24 | i1 << 16 | l << 8 | k;
			}
			else{
				int j1 = (int) ((float) (j >> 24 & 255) * red);
				int k1 = (int) ((float) (j >> 16 & 255) * green);
				int l1 = (int) ((float) (j >> 8 & 255) * blue);
				int a1 = (int) ((float) (j & 255) * alpha);
				//j = j & 255;
				j = j1 << 24 | k1 << 16 | l1 << 8 | a1;
			}
		}

		renderer.rawIntBuffer.put(i, j);
	}

}
