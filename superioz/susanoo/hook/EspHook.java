package de.superioz.susanoo.hook;

import de.superioz.susanoo.Client;
import de.superioz.susanoo.friend.FriendManager;
import de.superioz.susanoo.module.ModuleManager;
import de.superioz.susanoo.module.mods.render.ChestESP;
import de.superioz.susanoo.module.mods.render.EntityESP;
import de.superioz.susanoo.module.mods.render.PlayerESP;
import de.superioz.susanoo.module.mods.render.Trajectories;
import de.superioz.susanoo.util.Colors;
import de.superioz.susanoo.util.render.OutlineUtil;
import net.minecraft.client.model.ModelChest;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntityAmbientCreature;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.tileentity.TileEntityEnderChest;

import java.awt.*;

/**
 * Created on 21.06.2016.
 */
public class EspHook {

	//public static final java.util.List<Entity> TO_DRAW = new ArrayList<>();

	/**
	 * Renders the model
	 *
	 * @param entity   The entity
	 * @param renderer The renderer
	 * @param f17      1
	 * @param f16      2
	 * @param f14      3
	 * @param f12      4
	 * @param f20      5
	 */
	public static void renderEntityModel(EntityLivingBase entity, RendererLivingEntity renderer,
	                                     float f17, float f16, float f14, float f12, float f20){
		boolean entityesp = ModuleManager.getModule(EntityESP.class).isActive();
		boolean playeresp = ModuleManager.getModule(PlayerESP.class).isActive();
		boolean trajectories = ModuleManager.getModule(Trajectories.class).isActive();

		if(entityesp || playeresp || trajectories){
			GlStateManager.depthMask(true);

			//if(!(entity instanceof EntityPlayer) && !((EntityPlayer) entity).isSpectator()){
			//	this.renderLayers(entity, f6, f5, partialTicks, f8, f2, f7, f4);
			//}

			if((entity instanceof EntityPlayer && playeresp && entity != Client.mc.thePlayer)
					|| entityesp && entity != Client.mc.thePlayer
					|| trajectories && Trajectories.entityStack.contains(entity.getEntityId())){

				float idk = 0.0625F;

				renderer.mirrorRenderModel(entity, f17, f16, f14, f12, f20, idk);
				OutlineUtil.renderOne();
				renderer.mirrorRenderModel(entity, f17, f16, f14, f12, f20, idk);
				OutlineUtil.renderTwo();
				renderer.mirrorRenderModel(entity, f17, f16, f14, f12, f20, idk);
				OutlineUtil.renderThree();

				Color color;
				if(trajectories && Trajectories.entityStack.contains(entity.getEntityId())){
					color = Colors.ORANGE_0;
				}
				else{
					if(entity instanceof EntityMob){
						color = Colors.LIGHT_RED;
					}
					else if(entity instanceof EntityAnimal){
						color = Colors.LIGHT_GREEN;
					}
					else if(entity instanceof EntityAmbientCreature
							|| entity instanceof EntityPlayer){
						color = Colors.WHITE;
					}
					else{
						color = Colors.MAGENTA;
					}

					// The friend
					if(FriendManager.isFriend(entity)){
						color = Colors.LIGHT_GREEN;
					}
				}

				OutlineUtil.renderFour(color);
				renderer.mirrorRenderModel(entity, f17, f16, f14, f12, f20, idk);
				OutlineUtil.renderFive();

				if(trajectories && Trajectories.entityStack.contains(entity.getEntityId())){
					Trajectories.entityStack.remove(Trajectories.entityStack.indexOf(entity.getEntityId()));
				}

				return;
			}
		}
		renderer.mirrorRenderModel(entity, f17, f16, f14, f12, f20, 0.0625F);
	}

	/**
	 * Renders a chest model
	 *
	 * @param chest      The chest
	 * @param modelChest The model
	 */
	public static void renderChestModel(TileEntityChest chest, ModelChest modelChest){
		if(ModuleManager.getModule(ChestESP.class).isActive() && chest.getWorld() != null){
			modelChest.renderAll();
			OutlineUtil.renderOne();
			modelChest.renderAll();
			OutlineUtil.renderTwo();
			modelChest.renderAll();
			OutlineUtil.renderThree();
			OutlineUtil.renderFour(Colors.ORANGE_0);
			modelChest.renderAll();
			OutlineUtil.renderFive();
		}
		else{
			modelChest.renderAll();
		}
	}

	/**
	 * Renders a chest model
	 *
	 * @param chest      The chest
	 * @param modelChest The model
	 */
	public static void renderEnderChestModel(TileEntityEnderChest chest, ModelChest modelChest){
		if(ModuleManager.getModule(ChestESP.class).isActive() && chest.getWorld() != null){
			modelChest.renderAll();
			OutlineUtil.renderOne();
			modelChest.renderAll();
			OutlineUtil.renderTwo();
			modelChest.renderAll();
			OutlineUtil.renderThree();
			OutlineUtil.renderFour(Colors.MAGENTA);
			modelChest.renderAll();
			OutlineUtil.renderFive();
		}
		else{
			modelChest.renderAll();
		}
	}

}
