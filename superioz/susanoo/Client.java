package de.superioz.susanoo;

import de.superioz.susanoo.config.XConfigurationManager;
import de.superioz.susanoo.cosmetic.CapeManager;
import de.superioz.susanoo.event.EventHandler;
import de.superioz.susanoo.event.EventManager;
import de.superioz.susanoo.event.Listener;
import de.superioz.susanoo.event.events.ClientShutdownEvent;
import de.superioz.susanoo.event.events.ClientStartupEvent;
import de.superioz.susanoo.event.events.ClientUpdateEvent;
import de.superioz.susanoo.friend.FriendManager;
import de.superioz.susanoo.gui.GuiIngameOverlay;
import de.superioz.susanoo.gui.GuiMenu;
import de.superioz.susanoo.gui.OptionsModule;
import de.superioz.susanoo.gui.notification.Notification;
import de.superioz.susanoo.gui.notification.NotificationCenter;
import de.superioz.susanoo.module.ModuleManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.entity.boss.BossStatus;

/**
 * Created on 25.05.2016.
 */
public class Client implements Listener {

	public static final String CLIENT_NAME = "Susanoo";
	public static final String CLIENT_SPACER = " x ";

	public static final String CLIENT_VERSION = "b2";

	public static Minecraft mc = Minecraft.getMinecraft();
	public static FontRenderer fr = Minecraft.getMinecraft().fontRendererObj;
	public static GuiMenu gui;
	public static OptionsModule options;

	// Registers listener
	public Client(){
		EventManager.registerListener(this);
		EventManager.registerListener(new ModuleManager());
		EventManager.registerListener(new GuiIngameOverlay());
		EventManager.registerListener(new FriendManager());
	}

	@EventHandler
	public static void onStartup(ClientStartupEvent event){
		if(options == null){
			options = new OptionsModule();
		}

		ModuleManager.startup();
		XConfigurationManager.setup();
		CapeManager.init();

		if(gui == null){
			gui = new GuiMenu();
		}
	}

	@EventHandler
	public static void onShutdown(ClientShutdownEvent event){
		ModuleManager.shutdown();
		XConfigurationManager.shutdown();
	}

	@EventHandler
	public static void onUpdate(ClientUpdateEvent event){
		if(!options.activate.getState()){
			ModuleManager.shutdown();
		}
		if(!options.teamMates.getState()){
			if(FriendManager.clear() && isInGame()){
				NotificationCenter.addMessage(new Notification("\2474Resetted your friend list."));
			}
		}
	}

	/**
	 * Send message to player
	 *
	 * @param message The message
	 */
	public static void sendMessage(String message){
		if(isInGame())
			NotificationCenter.addMessage(new Notification(message));
	}

	public static boolean isInGame(){
		return (mc != null) && (mc.thePlayer != null);
	}

	public static boolean hasBossbar(){
		return BossStatus.bossName != null && BossStatus.statusBarTime > 0;
	}

}
