package de.superioz.susanoo.event;

/**
 * Created on 27.05.2016.
 */
public interface PacketChangeable
{
    boolean isPacketChanged();
    void setPacketChanged(boolean f);
}
