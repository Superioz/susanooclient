package de.superioz.susanoo.event.events;

import de.superioz.susanoo.event.Event;
import net.minecraft.entity.Entity;

/**
 * Created on 27.05.2016.
 */
public class EntityVelocityEvent extends Event {

	private Entity entity;
	private double x, y, z;

	public EntityVelocityEvent(Entity entity, double x, double y, double z){
		this.entity = entity;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Entity getEntity(){
		return entity;
	}

	public double getX(){
		return x;
	}

	public void setX(double x){
		this.x = x;
	}

	public double getY(){
		return y;
	}

	public void setY(double y){
		this.y = y;
	}

	public double getZ(){
		return z;
	}

	public void setZ(double z){
		this.z = z;
	}

}
