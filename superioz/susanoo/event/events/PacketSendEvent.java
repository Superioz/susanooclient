package de.superioz.susanoo.event.events;

import de.superioz.susanoo.event.Cancellable;
import de.superioz.susanoo.event.Event;
import de.superioz.susanoo.event.PacketChangeable;
import net.minecraft.network.Packet;

/**
 * Created on 27.05.2016.
 */
public class PacketSendEvent extends Event implements PacketChangeable, Cancellable
{
    private Packet packet;
    private boolean changed;
    private boolean cancelled;

    public PacketSendEvent(Packet packet)
    {
        this.packet = packet;
    }

    // Intern methods

    public Packet getPacket()
    {
        return packet;
    }

    public void setPacket(Packet packet)
    {
        this.packet = packet;
    }

    @Override
    public boolean isPacketChanged()
    {
        return changed;
    }

    @Override
    public void setPacketChanged(boolean f)
    {
        changed = f;
    }

    @Override
    public boolean isCancelled()
    {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b)
    {
        cancelled = b;
    }
}
