package de.superioz.susanoo.event.events;

import de.superioz.susanoo.event.Event;

/**
 * Created on 26.05.2016.
 */
public class ClientRenderEvent extends Event
{
    private float partialTicks;

    public ClientRenderEvent(float partialTicks)
    {
        this.partialTicks = partialTicks;
    }

    public float getPartialTicks()
    {
        return partialTicks;
    }
}
