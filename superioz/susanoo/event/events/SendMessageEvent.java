package de.superioz.susanoo.event.events;

import de.superioz.susanoo.event.Cancellable;
import de.superioz.susanoo.event.Event;

/**
 * Created on 27.05.2016.
 */
public class SendMessageEvent extends Event implements Cancellable
{
    private String s;
    private boolean cancelled = false;

    public SendMessageEvent(String s)
    {
        this.s = s;
    }

    public String getMessage()
    {
        return s;
    }

    @Override
    public boolean isCancelled()
    {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b)
    {
        cancelled = b;
    }
}
