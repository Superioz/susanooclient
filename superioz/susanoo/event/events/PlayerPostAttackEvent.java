package de.superioz.susanoo.event.events;

import de.superioz.susanoo.event.Event;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;

/**
 * Created on 02.06.2016.
 */
public class PlayerPostAttackEvent extends Event
{
    private EntityPlayer player;
    private Entity target;

    public PlayerPostAttackEvent(EntityPlayer player, Entity target)
    {
        this.player = player;
        this.target = target;
    }

    public EntityPlayer getPlayer()
    {
        return player;
    }

    public Entity getTarget()
    {
        return target;
    }
}
