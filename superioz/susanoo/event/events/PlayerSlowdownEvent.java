package de.superioz.susanoo.event.events;

import de.superioz.susanoo.event.Cancellable;
import de.superioz.susanoo.event.Event;

/**
 * Created on 27.05.2016.
 */
public class PlayerSlowdownEvent extends Event implements Cancellable {

	private boolean cancelled = false;

	@Override
	public boolean isCancelled(){
		return cancelled;
	}

	@Override
	public void setCancelled(boolean b){
		cancelled = b;
	}
}
