package de.superioz.susanoo.event.events;

import de.superioz.susanoo.event.Event;

/**
 * Created on 25.06.2016.
 */
public class PlayerMotionEvent extends Event {

	private double motionX;
	private double motionY;
	private double motionZ;

	public PlayerMotionEvent(double motionX, double motionY, double motionZ){
		this.motionX = motionX;
		this.motionY = motionY;
		this.motionZ = motionZ;
	}

	public double getMotionX(){
		return motionX;
	}

	public void setMotionX(double motionX){
		this.motionX = motionX;
	}

	public double getMotionY(){
		return motionY;
	}

	public void setMotionY(double motionY){
		this.motionY = motionY;
	}

	public double getMotionZ(){
		return motionZ;
	}

	public void setMotionZ(double motionZ){
		this.motionZ = motionZ;
	}

}
