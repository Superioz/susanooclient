package de.superioz.susanoo.event.events;

import de.superioz.susanoo.event.Event;

/**
 * Created on 26.05.2016.
 */
public class KeyPressEvent extends Event
{
    private int keyCode;

    public KeyPressEvent(int keyCode)
    {
        this.keyCode = keyCode;
    }

    public int getKeyCode()
    {
        return keyCode;
    }
}
