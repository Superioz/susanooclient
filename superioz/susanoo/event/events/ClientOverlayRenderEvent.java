package de.superioz.susanoo.event.events;

import de.superioz.susanoo.event.Event;
import net.minecraft.client.gui.GuiIngame;

/**
 * Created on 26.05.2016.
 */
public class ClientOverlayRenderEvent extends Event
{
    private GuiIngame gui;

    public ClientOverlayRenderEvent(GuiIngame gui)
    {
        this.gui = gui;
    }

    public GuiIngame getGui()
    {
        return gui;
    }
}
