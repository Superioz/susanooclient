package de.superioz.susanoo.event.events;

import de.superioz.susanoo.event.Event;

/**
 * Created on 01.06.2016.
 */
public class MouseClickEvent extends Event {

	private Type type;

	public MouseClickEvent(Type type){
		this.type = type;
	}

	public Type getType(){
		return type;
	}

	public enum Type {
		RIGHTCLICK,
		LEFTCLICK,
		MIDDLECLICK
	}
}
