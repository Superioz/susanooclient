package de.superioz.susanoo.event.events;

import de.superioz.susanoo.event.Cancellable;
import de.superioz.susanoo.event.Event;
import net.minecraft.network.Packet;

/**
 * Created on 02.06.2016.
 */
public class PacketReceiveEvent extends Event implements Cancellable
{
    public boolean cancelled;
    public Packet packet;

    public PacketReceiveEvent(Packet packet)
    {
        this.packet = packet;
    }

    public Packet getPacket()
    {
        return packet;
    }

    @Override
    public boolean isCancelled()
    {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b)
    {
        cancelled = b;
    }
}
