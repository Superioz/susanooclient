package de.superioz.susanoo.event.events;

import de.superioz.susanoo.event.Event;

/**
 * Created on 27.05.2016.
 */
public class RenderStringEvent extends Event
{
    private String s;

    public RenderStringEvent(String s)
    {
        this.s = s;
    }

    public String getString()
    {
        return s;
    }
}
