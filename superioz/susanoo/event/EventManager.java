package de.superioz.susanoo.event;

import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 26.05.2016.
 */
public class EventManager
{
    private static final List<Listener> REGISTERED_LISTENER = new ArrayList<Listener>();

    /**
     * Calls an event
     *
     * @param event The event
     */
    public static void callEvent(Event event)
    {
        for (Listener listener : REGISTERED_LISTENER)
        {
            Class c = listener.getClass();

            for (Method m : c.getDeclaredMethods())
            {
                if (!m.isAnnotationPresent(EventHandler.class))
                {
                    continue;
                }

                if (m.getParameterTypes().length > 1 || m.getParameterTypes().length < 0)
                {
                    continue;
                }

                Class<?> p = m.getParameterTypes()[0];

                if (p.getSuperclass().equals(Event.class)
                        && p == event.getClass())
                {
                    try
                    {
                        m.invoke(listener, event);
                    }
                    catch (IllegalAccessException e)
                    {
                        e.printStackTrace();
                    }
                    catch (InvocationTargetException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }

        for (Module m : ModuleManager.getModules())
        {
            if (!m.isActive())
            {
                continue;
            }

            m.onEvent(event);
        }
    }

    /**
     * Register a listener to list
     *
     * @param listener The listener
     */
    public static void registerListener(Listener listener)
    {
        if (REGISTERED_LISTENER.contains(listener))
        {
            return;
        }

        REGISTERED_LISTENER.add(listener);
    }

    public static List<Listener> getRegisteredListener()
    {
        return REGISTERED_LISTENER;
    }
}
