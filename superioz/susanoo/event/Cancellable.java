package de.superioz.susanoo.event;

/**
 * Created on 27.05.2016.
 */
public interface Cancellable
{
    boolean isCancelled();
    void setCancelled(boolean b);
}
