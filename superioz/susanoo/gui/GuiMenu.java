package de.superioz.susanoo.gui;

import de.superioz.susanoo.Client;
import de.superioz.susanoo.gui.obj.GuiConfigPair;
import de.superioz.susanoo.gui.obj.GuiToggleButton;
import de.superioz.susanoo.gui.tabs.ModulesPanel;
import de.superioz.susanoo.gui.tabs.OptionsPanel;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import de.superioz.susanoo.module.ModuleManager;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.util.Colors;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created on 27.05.2016.
 */
public class GuiMenu extends GuiFrame {

	public static final GridBagConstraints SCROLLPANE_CONSTR = new GridBagConstraints();

	public static final int WIDTH = 500;
	public static final int HEIGHT = 700;
	public static final int SCROLL_SPEED = 25;
	public static final int BASE_INSET = 5;

	/**
	 * Constructor for setting up the application
	 */
	public GuiMenu(){
		super();
		setType(Type.POPUP);
		setTitle(Client.CLIENT_NAME + Client.CLIENT_SPACER + "Menu");
		setResizable(false);
		//super.setLocationRelativeTo(null);
		setLocation(50, 50);
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setup();
		this.initUI();
		setVisible(true);
	}

	/**
	 * Sets smth up
	 */
	private void setup(){
		SCROLLPANE_CONSTR.anchor = GridBagConstraints.NORTHWEST;
		SCROLLPANE_CONSTR.insets = new Insets(BASE_INSET, BASE_INSET * 2, BASE_INSET, BASE_INSET);
		SCROLLPANE_CONSTR.fill = GridBagConstraints.NONE;
		SCROLLPANE_CONSTR.gridx = 0;
		SCROLLPANE_CONSTR.gridy = GridBagConstraints.RELATIVE;
		//SCROLLPANE_CONSTR.weightx = 1f;
		//SCROLLPANE_CONSTR.weighty = 1f;
	}

	/**
	 * Get gui config pairs
	 * Consists of the activate toggle button,
	 * the key code and eventually world settings (speed, RANGE, ..)
	 *
	 * @param m The module
	 * @return The list of gui config pairs
	 */
	public static java.util.List<GuiConfigPair> getGuiConfigPair(final Module m){
		String label0 = "Status:";
		GuiToggleButton btn0 = m.getActiveToggleButton();
		java.util.List<GuiConfigPair> components = new ArrayList<GuiConfigPair>();

		String label1 = "Key:";
		final JButton btn1 = m.getKeyCodeButton();
		components.add(new GuiConfigPair(label0, btn0.getBase()));
		components.add(new GuiConfigPair(label1, btn1));

		if(m instanceof ConfigSaveable){
			components.addAll(GuiConfigPair.build((ConfigSaveable) m,
					Colors.BLUE_1, Colors.ORANGE_1, "Enabled", "Disabled"));
		}
		return components;
	}

	/**
	 * Initialises the user interface
	 */
	private void initUI(){
		JTabbedPane tabbedPane = new JTabbedPane(SwingConstants.TOP);
		JTabbedPane childPaneMod = new JTabbedPane(SwingConstants.TOP);

		// Panels
		OptionsPanel optionsPanel = new OptionsPanel();

		for(ModuleCategory category : ModuleCategory.values()){
			ModulesPanel mp0 = new ModulesPanel(category);

			childPaneMod.add(category.getName(), mp0.getChildPane());
		}

		tabbedPane.add("Options", optionsPanel.getChildPane());
		tabbedPane.add("Modules [" + ModuleManager.getModules().size() + " Mod(s)]", childPaneMod);
		add(tabbedPane);
	}
}