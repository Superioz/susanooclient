package de.superioz.susanoo.gui.obj;

import de.superioz.susanoo.gui.GuiMenu;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 28.05.2016.
 */
public class GuiConfigSection {

	private JPanel parent;
	private String header;

	private JLabel headerLabel;
	private JPanel sectionPanel;

	private List<GuiConfigPair> subComponents;

	public GuiConfigSection(String header, JPanel parent){
		this.header = header.toUpperCase();
		this.parent = parent;
		this.subComponents = new ArrayList<>();
		GridLayout layout = new GridLayout(0, 1);
		layout.setVgap(10);
		this.sectionPanel = new JPanel(layout);
		this.sectionPanel.setMaximumSize(new Dimension(GuiMenu.WIDTH, GuiMenu.HEIGHT));
		this.headerLabel = new JLabel(this.header);
		this.headerLabel.setFont(this.headerLabel.getFont().deriveFont(0, 16f));
	}

	/**
	 * Builds this section
	 *
	 * @param subComponents The sub componenets
	 * @return The panel
	 */
	public JPanel build(List<GuiConfigPair> subComponents){
		if(subComponents == null){
			subComponents = this.subComponents;
		}
		else{
			this.subComponents = subComponents;
		}

		this.add(new GuiConfigPair(headerLabel, null));

		if(subComponents != null){
			for(GuiConfigPair p : subComponents){
				this.add(p);
			}
		}

		this.parent.add(sectionPanel, GuiMenu.SCROLLPANE_CONSTR);
		return sectionPanel;
	}

	/**
	 * Adds given component to a horizontal box and then to the vertical box from the
	 * configuration section
	 *
	 * @param cp The component pair
	 */
	private void add(GuiConfigPair cp){
		JPanel hbox = new JPanel(new GridLayout(0, 2));
		hbox.setMaximumSize(new Dimension(GuiMenu.WIDTH, GuiMenu.HEIGHT));
		hbox.setPreferredSize(new Dimension(GuiMenu.WIDTH - 75, 30));

		if(cp.getJlabel() != null){
			hbox.add(cp.getJlabel());
		}

		if(cp.getComponent() != null){
			hbox.add(cp.getComponent());
		}

		sectionPanel.add(hbox);
	}

	// Intern methods

	public String getHeader(){
		return header;
	}

	public Component getParent(){
		return parent;
	}

	public JPanel getSectionPanel(){
		return sectionPanel;
	}

	public JLabel getHeaderLabel(){
		return headerLabel;
	}

	public List<GuiConfigPair> getSubComponents(){
		return subComponents;
	}

	public void setSubComponents(List<GuiConfigPair> subComponents){
		this.subComponents = subComponents;
	}
}
