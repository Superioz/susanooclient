package de.superioz.susanoo.gui.obj;

import de.superioz.susanoo.module.spec.RangeValue;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Created on 29.05.2016.
 */
public class GuiRangeSlider {

	private JLabel label;
	private JSlider slider;
	private RangeValue value;
	private int current;
	private ChangeListener listener;

	public GuiRangeSlider(RangeValue value, JSlider base, ChangeListener listener){
		this.value = value;
		this.slider = base;
		this.listener = listener;
		this.label = new JLabel();
		this.setSliderDefaults();
	}

	private void setSliderDefaults(){
		// Values
		slider.setBounds(35, 0, 50, 5);
		slider.setFont(slider.getFont().deriveFont(12f));
		slider.setMinimum(value.getFrom());
		slider.setMaximum(value.getTo());
		slider.setMajorTickSpacing(value.getDiff());
		slider.setMinorTickSpacing(value.getDiff());
		slider.setPaintTrack(true);
		slider.setSnapToTicks(true);
		slider.addChangeListener(listener);

		if(value.getDef() != value.getCurrent()){
			slider.setValue(value.getCurrent());
		}
		else{
			slider.setValue(value.getDef());
		}
		label.setBounds(0, 0, 20, 5);
		label.setText(value.getText(slider.getValue()));
		label.setVisible(true);

		slider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e){
				JSlider slider = (JSlider) e.getSource();
				int after = slider.getValue();

				label.setText(value.getText(after));
			}
		});
	}

	public JSlider getBase(){
		return slider;
	}

	public RangeValue getValue(){
		return value;
	}

	public void setBase(JSlider slider){
		this.slider = slider;
	}

	public void setValue(RangeValue value){
		this.value = value;
	}

	public int getCurrent(){
		return current;
	}

	public void setCurrent(int current){
		this.current = current;
	}

	public JLabel getLabel(){
		return label;
	}
}
