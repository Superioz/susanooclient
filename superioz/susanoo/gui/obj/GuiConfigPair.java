package de.superioz.susanoo.gui.obj;

import de.superioz.susanoo.module.spec.*;
import de.superioz.susanoo.util.impl.Consumer;
import de.superioz.susanoo.util.impl.SerializablePair;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created on 28.05.2016.
 */
public class GuiConfigPair {

	private String label;
	private JLabel jlabel;

	private Component component;

	public GuiConfigPair(JLabel label, Component component){
		this.jlabel = label;
		this.label = label.getText();
		this.component = component;
	}

	public GuiConfigPair(String label, Component component){
		this(new JLabel(label), component);
		this.jlabel.setFont(jlabel.getFont().deriveFont(12f));
	}

	/**
	 * Builds a list of guiconfigpairs with saveable
	 *
	 * @param saveable The saveable
	 * @return The list
	 */
	public static java.util.List<GuiConfigPair> build(final ConfigSaveable saveable,
	                                                  final Color bColor1, final Color bColor2,
	                                                  final String bEnabled, final String bDisabled){
		final HashMap<Integer, SerializablePair<String, ConfigValue>> values = saveable.getConfigValues();
		java.util.List<GuiConfigPair> components = new ArrayList<GuiConfigPair>();

		for(int i : values.keySet()){
			SerializablePair<String, ConfigValue> value = values.get(i);
			String label = value.getFirst();
			final ConfigValue v = value.getSecond();

			if(v instanceof RangeValue){
				final JSlider base = new JSlider();

				// Slider
				Box hbox = new Box(BoxLayout.X_AXIS);

				GuiRangeSlider slider = new GuiRangeSlider((RangeValue) v, base, new ChangeListener() {
					@Override
					public void stateChanged(ChangeEvent e){
						RangeValue rangeValue = (RangeValue) v;
						rangeValue.setCurr(base.getValue());
						saveable.setConfigValues(values);
					}
				});
				hbox.add(slider.getLabel());
				hbox.add(Box.createRigidArea(new Dimension(10, 0)));
				hbox.add(slider.getBase());

				components.add(new GuiConfigPair(label, hbox));
			}
			else if(v instanceof ToggleValue){
				JToggleButton base = new JToggleButton();

				// Button
				GuiToggleButton button = new GuiToggleButton(
						base, (ToggleValue) v, bEnabled, bDisabled, bColor1, bColor2,
						new Consumer<GuiToggleButton.WrappedActionEvent>() {
							@Override
							public void consume(GuiToggleButton.WrappedActionEvent object){
								saveable.setConfigValues(values);
							}
						});
				components.add(new GuiConfigPair(label, button.getBase()));
			}
			else if(v instanceof ListValue){
				JToggleButton base = new JToggleButton();

				GuiListButton button = new GuiListButton(base, (ListValue) v,
						new Consumer<GuiListButton.WrappedActionEvent>() {
							@Override
							public void consume(GuiListButton.WrappedActionEvent object){
								saveable.setConfigValues(values);
							}
						});
				components.add(new GuiConfigPair(label, button.getBase()));
			}
		}
		return components;
	}

	// Intern methods

	public String getLabel(){
		return label;
	}

	public void setLabel(String label){
		this.label = label;
		this.jlabel.setText(label);
	}

	public JLabel getJlabel(){
		return jlabel;
	}

	public void setJlabel(JLabel jlabel){
		this.jlabel = jlabel;
	}

	public Component getComponent(){
		return component;
	}

	public void setComponent(Component component){
		this.component = component;
	}
}
