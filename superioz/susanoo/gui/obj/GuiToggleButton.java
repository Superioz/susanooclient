package de.superioz.susanoo.gui.obj;

import de.superioz.susanoo.event.Cancellable;
import de.superioz.susanoo.module.spec.ToggleValue;
import de.superioz.susanoo.util.impl.Consumer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created on 03.06.2016.
 */
public class GuiToggleButton {

	private GuiToggleButton instance;

	private JToggleButton base;
	private boolean state = false;
	private ToggleValue value;

	private String s_activated, s_deactivated;
	private Color c_activated, c_deactivated;

	private Consumer<WrappedActionEvent> listener;

	public GuiToggleButton(final JToggleButton base, final ToggleValue value,
	                       String s_activated, String s_deactivated,
	                       Color c_activated, Color c_deactivated,
	                       Consumer<WrappedActionEvent> consumer){
		this.instance = this;
		this.base = base;
		this.base.setFocusable(false);
		this.value = value;
		this.s_activated = s_activated;
		this.s_deactivated = s_deactivated;
		this.c_activated = c_activated;
		this.c_deactivated = c_deactivated;
		this.listener = consumer;

		if(value.getState() != value.getDefault()){
			state = value.getState();
		}
		else{
			state = value.getDefault();
		}

		this.setState(state);
		this.base.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
				base.setSelected(false);
				WrappedActionEvent event = new WrappedActionEvent(e, instance);

				if(listener != null){
					listener.consume(event);
				}

				if(!event.isCancelled()){
					setState(!state);
				}
			}
		});
	}

	/**
	 * Set the state
	 *
	 * @param state The state
	 */
	public void setState(boolean state){
		this.state = state;
		value.setState(this.state);
		String s = value.getState() ? s_activated : s_deactivated;
		Color c = value.getState() ? c_activated : c_deactivated;
		base.setText(s.toUpperCase());
		base.setToolTipText(base.getText());
		base.setForeground(c);
		base.setSelected(false);
	}

	// Intern methods

	public GuiToggleButton setListener(Consumer<WrappedActionEvent> consumer){
		this.listener = consumer;
		return this;
	}

	public JToggleButton getBase(){
		return base;
	}

	public boolean isState(){
		return state;
	}

	public ToggleValue getValue(){
		return value;
	}

	public String getActivatedString(){
		return s_activated;
	}

	public String getDeactivatedString(){
		return s_deactivated;
	}

	public Color getActivatedColor(){
		return c_deactivated;
	}

	public Color getDeactivatedColor(){
		return c_activated;
	}

	/**
	 * Created on 03.06.2016.
	 */
	public static class WrappedActionEvent implements Cancellable {

		private ActionEvent event;
		private GuiToggleButton button;
		private boolean cancelled = false;

		public WrappedActionEvent(ActionEvent event, GuiToggleButton button){
			this.event = event;
			this.button = button;
		}

		public ActionEvent getEvent(){
			return event;
		}

		public GuiToggleButton getButton(){
			return button;
		}

		@Override
		public boolean isCancelled(){
			return cancelled;
		}

		@Override
		public void setCancelled(boolean b){
			cancelled = b;
		}
	}
}
