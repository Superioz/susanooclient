package de.superioz.susanoo.gui.obj;

import de.superioz.susanoo.event.Cancellable;
import de.superioz.susanoo.module.spec.ListValue;
import de.superioz.susanoo.util.Colors;
import de.superioz.susanoo.util.impl.Consumer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created on 22.06.2016.
 */
public class GuiListButton {

	private JToggleButton base;
	private ListValue listValue;
	private Consumer<WrappedActionEvent> listener;

	public GuiListButton(final JToggleButton base, final ListValue listValue,
	                     final Consumer<WrappedActionEvent> listener){
		this.base = base;
		this.base.setFocusable(false);
		this.listValue = listValue;
		this.listener = listener;

		this.base.setForeground(Colors.LIST_VALUE);
		this.updateText();
		this.base.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
				base.setSelected(false);
				WrappedActionEvent event = new WrappedActionEvent(e, GuiListButton.this);

				if(listener != null){
					listener.consume(event);
				}
				if(event.isCancelled()) return;

				listValue.next();
				updateText();
			}
		});
	}

	/**
	 * Updates the text
	 */
	public void updateText(){
		this.base.setText((listValue.get() + "").toUpperCase());
		this.base.setToolTipText(base.getText());
	}

	// Intern

	public JToggleButton getBase(){
		return base;
	}

	public ListValue getListValue(){
		return listValue;
	}

	public Consumer<WrappedActionEvent> getListener(){
		return listener;
	}

	/**
	 * Created on 03.06.2016.
	 */
	public static class WrappedActionEvent implements Cancellable {

		private ActionEvent event;
		private GuiListButton button;
		private boolean cancelled = false;

		public WrappedActionEvent(ActionEvent event, GuiListButton button){
			this.event = event;
			this.button = button;
		}

		public ActionEvent getEvent(){
			return event;
		}

		public GuiListButton getButton(){
			return button;
		}

		@Override
		public boolean isCancelled(){
			return cancelled;
		}

		@Override
		public void setCancelled(boolean b){
			cancelled = b;
		}
	}

}
