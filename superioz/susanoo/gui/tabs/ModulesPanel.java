package de.superioz.susanoo.gui.tabs;

import de.superioz.susanoo.gui.obj.GuiConfigPair;
import de.superioz.susanoo.gui.obj.GuiConfigSection;
import de.superioz.susanoo.gui.GuiMenu;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleCategory;
import de.superioz.susanoo.module.ModuleManager;

import javax.swing.*;

/**
 * Created on 28.05.2016.
 */
public class ModulesPanel extends GuiMenuTab {

	public ModulesPanel(ModuleCategory category){
		super(category);
	}

	@Override
	public void init(JPanel parent){
		for(Module m : ModuleManager.getModules()){
			if(m.getCategory() != category){
				continue;
			}

			GuiConfigSection section = new GuiConfigSection(m.getName(), this);
			java.util.List<GuiConfigPair> components = GuiMenu.getGuiConfigPair(m);
			section.build(components);
		}
	}
}
