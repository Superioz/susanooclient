package de.superioz.susanoo.gui.tabs;

import de.superioz.susanoo.Client;
import de.superioz.susanoo.gui.OptionsModule;
import de.superioz.susanoo.gui.obj.GuiConfigPair;
import de.superioz.susanoo.gui.obj.GuiConfigSection;
import de.superioz.susanoo.util.Colors;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 28.05.2016.
 */
public class OptionsPanel extends GuiMenuTab {

	public OptionsPanel(){
		super(null);
	}

	@Override
	public void init(JPanel parent){
		OptionsModule options = Client.options;
		GuiConfigSection section = new GuiConfigSection(OptionsModule.SECTION_NAME, this);
		section.build(getGuiConfigPair(options));
	}

	/**
	 * Get the config pair
	 *
	 * @param module The module
	 * @return The list
	 */
	public List<GuiConfigPair> getGuiConfigPair(final OptionsModule module){
		if(module == null) return new ArrayList<>();
		return GuiConfigPair.build(module, Colors.GREEN, Colors.RED, "Activated", "Deactivated");
	}
}
