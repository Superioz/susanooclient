package de.superioz.susanoo.gui.tabs;

import de.superioz.susanoo.gui.GuiMenu;
import de.superioz.susanoo.module.ModuleCategory;

import javax.swing.*;
import java.awt.*;

/**
 * Created on 28.05.2016.
 */
public abstract class GuiMenuTab extends JPanel
{
    protected JScrollPane childPane;
    protected ModuleCategory category;

    public GuiMenuTab(ModuleCategory category)
    {
        super();
        this.category = category;
        setLayout(new GridBagLayout());
        setMaximumSize(new Dimension(GuiMenu.WIDTH, GuiMenu.HEIGHT));
        childPane = new JScrollPane();
        childPane.setMaximumSize(new Dimension(WIDTH, HEIGHT));

        // Initialise objects
        this.init(this);
        childPane.setViewportView(this);
        childPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        childPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        childPane.getVerticalScrollBar().setUnitIncrement(GuiMenu.SCROLL_SPEED);
    }

    public abstract void init(JPanel parent);

    public JScrollPane getChildPane()
    {
        return childPane;
    }
}
