package de.superioz.susanoo.gui;

import de.superioz.susanoo.Client;
import de.superioz.susanoo.event.EventHandler;
import de.superioz.susanoo.event.Listener;
import de.superioz.susanoo.event.events.ClientOverlayRenderEvent;
import de.superioz.susanoo.gui.notification.NotificationCenter;
import de.superioz.susanoo.gui.overlay.*;
import de.superioz.susanoo.module.mods.other.ArrowPredictor;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.util.EnumFacing;

import static de.superioz.susanoo.gui.overlay.ModuleRenderer.DIFFERENCE_0;

/**
 * Created on 26.05.2016.
 */
public class GuiIngameOverlay extends GuiScreen implements Listener {

	@EventHandler
	public void onRender(ClientOverlayRenderEvent event){
		if(!Client.options.activate.getState()
				|| !Client.options.hud.getState()){
			return;
		}

		if(mc == null){
			mc = Minecraft.getMinecraft();
		}
		ScaledResolution res = new ScaledResolution(Client.mc, Client.mc.displayWidth, Client.mc.displayHeight);

		// Modules Header
		ModuleRenderer.renderHeader(this);

		// FPS
		if(Client.options.fps.getState()){
			int fpsBaseX = res.getScaledWidth();
			String fpsText = Minecraft.getDebugFPS() + "";
			int fpsX = fpsBaseX - Client.mc.fontRendererObj.getStringWidth(fpsText) - 35;

			Client.fr.drawStringWithShadow("\247fFPS:", fpsX,
					DIFFERENCE_0, ModuleRenderer.HEADER_COLOR);
			Client.fr.drawStringWithShadow("\2479" + fpsText, fpsX + 25, DIFFERENCE_0,
					ModuleRenderer.HEADER_COLOR);
		}

		// Radar
		if(Client.options.radar.getState()){
			RadarRenderer.drawRadar(this);
		}

		// Potion Effects
		if(Client.options.effects.getState()){
			PotionEffectRenderer.drawPotionEffects(res, this);
		}

		// Armor
		if(Client.options.armor.getState()){
			ArmorRenderer.renderArmor(res);
		}

		// Modules
		ModuleRenderer.render();

		// Notifications
		NotificationCenter.renderMessages(res, this);

		// Draw arrow detector
		EnumFacing playerFacing = mc.thePlayer.getHorizontalFacing();

		for(EntityArrow arrow : ArrowPredictor.couldHitList){
			EnumFacing arrowFacing = arrow.getHorizontalFacing();
			EnumFacing direction;

			if(playerFacing == arrowFacing){
				direction = EnumFacing.NORTH;
			}
			else if(playerFacing.getOpposite() == arrowFacing){
				direction = EnumFacing.SOUTH;
			}
			else if(playerFacing.rotateY() == arrowFacing){
				direction = EnumFacing.EAST;
			}
			else{
				direction = EnumFacing.WEST;
			}

			ArrowPredictionRenderer.renderPrediction(direction, res);
		}
		ArrowPredictor.couldHitList.clear();
	}

}
