package de.superioz.susanoo.gui.overlay;

import de.superioz.susanoo.Client;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 19.06.2016.
 */
public class ArmorRenderer {

	/**
	 * Render armor for player
	 *
	 * @param sc     The resolution
	 */
	public static void renderArmor(ScaledResolution sc){
		int yOffset = (mc.currentScreen instanceof GuiChat) ? 10 : 0;
		RenderItem itemRenderer = Client.mc.getRenderItem();

		List<ItemStack> items = new ArrayList<ItemStack>();
		items.addAll(Arrays.asList(Client.mc.thePlayer.inventory.armorInventory));

		int xOffset1 = 0;
		int xOffset2 = 0;

		int counter = 0;

		for(ItemStack stack : items){
			boolean first = counter >= 2;

			if(stack != null){
				//int x = sc.getScaledWidth() / 2 + 78 - xOffset;
				int x = sc.getScaledWidth() / 2 + (first ? -114 : 95);
				//int y = sc.getScaledHeight() - 55 - yOffset / 2;
				int y = sc.getScaledHeight() - 22 - (first ? xOffset1 : xOffset2) - yOffset + (first ? 0 : 2);

				itemRenderer.renderItemIntoGUI(stack, x, y);

				drawDamageInfo(sc, stack, x, y, counter);
			}

			if(first){
				xOffset1 += 19;
			}
			else{
				xOffset2 += 19;
			}
			counter++;
		}
	}

	/**
	 * Draw rdamage info
	 *
	 * @param sc    The sc
	 * @param stack The stack
	 */
	private static void drawDamageInfo(ScaledResolution sc, ItemStack stack, int x, int y, int counter){
		int rDamage = stack.getMaxDamage() - stack.getItemDamage();
		int maxDamage = stack.getMaxDamage();
		int percent = (int)(((double)rDamage/maxDamage) * 100);

		String damageString = percent + "%";

		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glScalef(0.5F, 0.5F, 0.5F);

		String color;
		if(percent > 70) color = "a";
		else if(percent < 30) color = "c";
		else color = "6";


		Client.mc.fontRendererObj.drawStringWithShadow("\247" + color + damageString, x * 2 + 15, y * 2 + 19
				+ (counter == 1 || counter == 2 ? 4 : 0), 16777215);

		GL11.glScalef(2.0F, 2.0F, 2.0F);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
	}

}
