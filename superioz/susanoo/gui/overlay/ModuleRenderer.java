package de.superioz.susanoo.gui.overlay;

import de.superioz.susanoo.Client;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleManager;
import net.minecraft.client.gui.Gui;
import org.lwjgl.input.Keyboard;

import java.util.List;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 20.06.2016.
 */
public class ModuleRenderer {

	public static final int DEFAULT_COLOR = 0xffcccccc;
	public static final int HEADER_COLOR = 0xffffffff;

	public static final int DIFFERENCE = 10;
	public static final int DIFFERENCE_0 = 4;
	public static final int DIFFERENCE_1 = 8;

	private static final int MAX_MODULES = 35;
	private static final int MAX_CHAR_SIZE = 14;

	private static final String HEADER = Client.CLIENT_NAME + Client.CLIENT_SPACER + "Client";

	public static void renderHeader(Gui gui){
		String header = "\247f" + HEADER + " \2477(\2479" + "ver" + "-" + Client.CLIENT_VERSION + "\2477)";

		gui.drawGradientRect(0, DIFFERENCE_0 - 2, DIFFERENCE_0 + mc.fontRendererObj.getStringWidth(header) + 3,
				DIFFERENCE_0 + mc.fontRendererObj.FONT_HEIGHT + 1, 0x60000000, 0x60000000);
		Client.fr.drawStringWithShadow(header, DIFFERENCE_0, DIFFERENCE_0, HEADER_COLOR);
	}

	public static void render(){
		List<Module> modules = ModuleManager.getModules();
		int diff = 10;

		for(int i = 0; i < MAX_MODULES; i++){
			if(i >= modules.size()){
				break;
			}

			Module m = modules.get(i);

			//
			if(!m.isActive()){
				continue;
			}

			int x = DIFFERENCE_0;
			int y = DIFFERENCE_1;
			//
			String name = m.getName();

			if(name.length() > MAX_CHAR_SIZE){
				name = name.substring(0, MAX_CHAR_SIZE);
			}

			//
			mc.fontRendererObj.drawStringWithShadow(">", x, y + diff, DEFAULT_COLOR);
			mc.fontRendererObj.drawStringWithShadow(name, x * 3, y + diff, m.getCategory().getColor());
			mc.fontRendererObj.drawStringWithShadow("(" + Keyboard.getKeyName(m.getKeyCode()) + ")",
					DIFFERENCE_0 * 24 - 5f, DIFFERENCE_1 + diff, DEFAULT_COLOR);
			diff += DIFFERENCE;
		}
	}

}
