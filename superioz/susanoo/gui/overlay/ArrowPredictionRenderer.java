package de.superioz.susanoo.gui.overlay;

import de.superioz.susanoo.util.render.RenderUtil;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.util.EnumFacing;
import org.lwjgl.opengl.GL11;

/**
 * Created on 24.06.2016.
 */
public class ArrowPredictionRenderer {

	/**
	 * Renders the arrow prediction
	 *
	 * @param facing The facing
	 * @param res    The resolution
	 */
	public static void renderPrediction(EnumFacing facing, ScaledResolution res){
		GlStateManager.disableTexture2D();
		GlStateManager.enableBlend();
		GlStateManager.disableAlpha();
		GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
		GlStateManager.shadeModel(7425);

		double baseX = res.getScaledWidth() / 2;
		double baseY = res.getScaledHeight() / 2;
		double zLevel = 0;

		double offset = 21;
		double minX = 0, minY = 0, maxX = 0, maxY = 0;

		boolean north = facing == EnumFacing.NORTH;
		boolean south = facing == EnumFacing.SOUTH;
		boolean east = facing == EnumFacing.EAST;
		//boolean west = facing == EnumFacing.WEST;

		Tessellator tessellator = RenderUtil.getTessellator();
		WorldRenderer renderer = RenderUtil.getRenderer();

		renderer.startDrawing(GL11.GL_POLYGON);
		renderer.setColorRGBA_F(1F, 0.2F, 0.1F, 0.5F);

		if(north || south){
			minX = baseX + (north ? -20 : 20);
			maxX = baseX + (north ? 20 : -20);
			minY = baseY + (north ? -offset : offset);
			maxY = minY + (north ? -12 : 12);

			renderer.addVertex(minX, minY, zLevel);
			renderer.addVertex(maxX, minY, zLevel);
			renderer.addVertex((minX + maxX) / 2, maxY, zLevel);
		}
		else{
			minX = baseX + (east ? offset : -offset);
			maxX = minX + (east ? 12 : -12);
			minY = baseY - (east ? 20 : -20);
			maxY = baseY + (east ? 20 : -20);

			renderer.addVertex(minX, minY, zLevel);
			renderer.addVertex(minX, maxY, zLevel);
			renderer.addVertex(maxX, (minY + maxY) / 2, zLevel);
		}

		tessellator.draw();

		GlStateManager.shadeModel(7424);
		GlStateManager.disableBlend();
		GlStateManager.enableAlpha();
		GlStateManager.enableTexture2D();
	}

}
