package de.superioz.susanoo.gui.overlay;

import de.superioz.susanoo.Client;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ResourceLocation;

import java.util.Collection;
import java.util.Iterator;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 19.06.2016.
 */
public class PotionEffectRenderer {

	/**
	 * Draw potion effects
	 *
	 * @param sc The scaled resolution
	 */
	public static void drawPotionEffects(ScaledResolution sc, GuiScreen screen){
		int yOffset = (mc.currentScreen instanceof GuiChat) ? sc.getScaledHeight() - 45 : sc.getScaledHeight() - 35;

		renderPotionEffects(screen, sc.getScaledWidth() + 10, yOffset);
	}

	/**
	 * Render potion effects
	 *
	 * @param screen The screen
	 * @param baseX  The base x
	 * @param baseY  The base y
	 */
	private static void renderPotionEffects(GuiScreen screen, int baseX, int baseY){
		int x = baseX - 124;
		int y = baseY;
		Collection activeEffects = Client.mc.thePlayer.getActivePotionEffects();

		if(!activeEffects.isEmpty()){
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			GlStateManager.disableLighting();
			int offset = 33 - 10;

//			if(activeEffects.size() > 5){
//				offset = 132 / (activeEffects.size() - 1);
//			}

			for(Iterator var6 = Client.mc.thePlayer.getActivePotionEffects().iterator(); var6.hasNext(); y -= offset){
				PotionEffect potionEffect = (PotionEffect) var6.next();
				Potion potion = Potion.potionTypes[potionEffect.getPotionID()];

				GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
				ResourceLocation inventoryBackground = new ResourceLocation("textures/gui/container/inventory.png");
				Client.mc.getTextureManager().bindTexture(inventoryBackground);
				screen.drawGradientRect(x - 2, y + 4, x + 120, y + 26, 0x90000000, 0x90000000);
				//screen.drawTexturedModalRect(x, y, 0, 166, 140, 32);

				if(potion.hasStatusIcon()){
					int statusIconIndex = potion.getStatusIconIndex();
					screen.drawTexturedModalRect(x + 6, y + 7, statusIconIndex % 8 * 18, 198 + statusIconIndex / 8 * 18, 18, 18);
				}

				String potionName = I18n.format(potion.getName(), new Object[0]);

				if(potionEffect.getAmplifier() == 1){
					potionName = potionName + " " + I18n.format("enchantment.level.2", new Object[0]);
				}
				else if(potionEffect.getAmplifier() == 2){
					potionName = potionName + " " + I18n.format("enchantment.level.3", new Object[0]);
				}
				else if(potionEffect.getAmplifier() == 3){
					potionName = potionName + " " + I18n.format("enchantment.level.4", new Object[0]);
				}

				Client.mc.fontRendererObj.drawStringWithShadow(potionName, x + 10 + 18, y + 6, 16777215);
				String durationString = Potion.getDurationString(potionEffect);
				Client.mc.fontRendererObj.drawStringWithShadow(durationString, x + 10 + 18, y + 6 + 10, 8355711);
			}
		}
	}

}
