package de.superioz.susanoo.gui.overlay;

import de.superioz.susanoo.Client;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.util.MathHelper;

/**
 * Created on 19.06.2016.
 */
public class RadarRenderer {

	private static final int DEFAULT_COLOR = 0xffcccccc;
	private static final int Y_DIFFERENCE_0 = 4;

	/**
	 * Code from LabyMod :P
	 */
	public static void drawRadar(GuiScreen screen){
		int i = screen.width / 2;
		int j = (int) -(MathHelper.wrapAngleTo180_float(Client.mc.thePlayer.rotationYaw)) - 360;
		int k = 45;

		for(int i1 = 0; i1 <= 2; i1++){
			for(double d0 = 0.0D; d0 <= 3.5D; d0 += 0.5D){
				if((j > i - 50) && j < i + 50){
					String s = "South";

					if(d0 == 0.5D){
						s = "South/West";
					}

					if(d0 == 1.0D){
						s = "West";
					}

					if(d0 == 1.5D){
						s = "West/North";
					}

					if(d0 == 2.0D){
						s = "North";
					}

					if(d0 == 2.5D){
						s = "North/East";
					}

					if(d0 == 3.0D){
						s = "East";
					}

					if(d0 == 3.5D){
						s = "East/South";
					}

					if(("" + d0).endsWith(".5")){
						s = "" + s;
					}
					else{
						s = "\247f" + s;
					}

					drawCenteredString("" + s, j, DEFAULT_COLOR);
				}

				j += k;
			}
		}
	}

	public static void drawCenteredString(String text, int x, int color){
		int fX = (x - Client.fr.getStringWidth(text) / 2);
		int y = Y_DIFFERENCE_0;

		if (Client.hasBossbar()){
			y += 17;
		}
		Client.fr.drawStringWithShadow(text, fX + getWidth() / 2, y, color);
	}

	public static int getHeight(){
		ScaledResolution scaledresolution = new ScaledResolution(Client.mc, Client.mc.displayWidth, Client.mc.displayHeight);
		return scaledresolution.getScaledHeight();
	}

	public static int getWidth(){
		ScaledResolution scaledresolution = new ScaledResolution(Client.mc, Client.mc.displayWidth, Client.mc.displayHeight);
		return scaledresolution.getScaledWidth();
	}

}
