package de.superioz.susanoo.gui.notification;

import de.superioz.susanoo.Client;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 20.06.2016.
 */
public class Notification {

	private String text;
	private State state;

	private long delay = -1;
	private int offsetCounter = 0;
	private int maxPosition;
	private int minPosition;

	private int currentX, currentY;
	public boolean someonesWaiting = false;

	public Notification(String text){
		this.text = text;
		this.minPosition = -(2 * (mc.fontRendererObj.FONT_HEIGHT - 2));
		this.offsetCounter = minPosition;
		this.currentY = minPosition;
		this.state = State.WAITING;

		if(Client.hasBossbar()){
			this.maxPosition = 25;
		}
		else{
			this.maxPosition = 15;
		}
	}

	/**
	 * Updates the notification
	 *
	 * @param screen   The screen
	 * @param oldY     The oldY
	 * @param newY     The newY
	 * @return If the notification moves
	 */
	public boolean update(GuiScreen screen, ScaledResolution res, int oldY, int newY){
		if(someonesWaiting && offsetCounter >= maxPosition){
			state = State.FINISHED;
			return false;
		}

		this.currentX = (res.getScaledWidth()/2) - (Client.mc.fontRendererObj.getStringWidth(text)/2);
		this.drawBackground(screen, oldY, newY);
		this.drawString(oldY);

		// Idle
		if(offsetCounter >= maxPosition){
			if(delay == -1){
				state = State.IDLE;
				this.startTimer();
				return false;
			}
			if(!this.checkDelayTime(1000 * 2)){
				return false;
			}

			resetTimer();
			state = State.MOVING_BACK;
		}

		// Move
		if(!(delay == -1 || checkDelayTime(7))){
			return false;
		}
		if(offsetCounter <= minPosition && state == State.MOVING_BACK){
			state = State.FINISHED;
		}
		if(state == State.MOVING_BACK){
			decreaseOffsetYCounter(1);
		}
		else{
			increaseOffsetYCounter(1);
		}
		startTimer();
		return true;
	}

	/**
	 * Draws the string
	 *
	 * @param oldY The old y
	 */
	private void drawString(int oldY){
		int color = 16777215;

		mc.fontRendererObj.drawStringWithShadow(text, currentX + 1, oldY + 2 + (offsetCounter * 2), color);
	}

	/**
	 * Draws the background of the update
	 *
	 * @param screen The screen
	 * @param oldY   The oldy
	 * @param newY   The newy
	 */
	private void drawBackground(GuiScreen screen, int oldY, int newY){
		int yOffset = offsetCounter * 2;
		int xOffset = Client.mc.fontRendererObj.getStringWidth(getText()) + 2;
		int color = 0x60000000;

		screen.drawGradientRect(currentX, oldY + yOffset, currentX + xOffset, newY + yOffset, color, color);
	}

	/**
	 * Checks the delay time
	 *
	 * @param millis The max delay time
	 * @return The result
	 */
	public boolean checkDelayTime(long millis){
		return System.currentTimeMillis() - delay > millis;
	}

	/**
	 * Starts the timer
	 */
	private void startTimer(){
		delay = System.currentTimeMillis();
	}

	/**
	 * Resets the timer
	 */
	private void resetTimer(){
		delay = -1;
	}

	// Intern method

	public String getText(){
		return text;
	}

	public int getCurrentX(){
		return currentX;
	}

	public int getCurrentY(){
		return currentY;
	}

	public void increaseOffsetYCounter(int pro){
		this.offsetCounter += pro;
	}

	public void decreaseOffsetYCounter(int pro){
		this.offsetCounter -= pro;
	}

	public State getState(){
		return state;
	}

	public enum State {

		MOVING_IN,
		MOVING_BACK,
		IDLE,
		WAITING,
		FINISHED

	}

}
