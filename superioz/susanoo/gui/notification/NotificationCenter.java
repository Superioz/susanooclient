package de.superioz.susanoo.gui.notification;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;

import java.util.Stack;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 20.06.2016.
 */
public class NotificationCenter {

	private static Stack<Notification> notifications = new Stack<>();
	private static Notification current;

	/**
	 * Adds a message
	 *
	 * @param notification The notification
	 */
	public static void addMessage(Notification notification){
		if(current != null){
			current.someonesWaiting = true;
		}
		notifications.push(notification);
	}

	/**
	 * Get the current message
	 *
	 * @return The string
	 */
	public static Notification getCurrentMessage(){
		if(current != null) return current;
		if(notifications.isEmpty()) return null;
		current = notifications.pop();
		return current;
	}

	/**
	 * Render messages
	 *
	 * @param res    The res
	 * @param screen The screen
	 */
	public static void renderMessages(ScaledResolution res, GuiScreen screen){
		int y = -(2 * (mc.fontRendererObj.FONT_HEIGHT - 2));
		int newY = y + mc.fontRendererObj.FONT_HEIGHT + 2;

		// Update position
		Notification notification = getCurrentMessage();
		if(notification == null) return;

		notification.update(screen, res, y, newY);
		if(notification.getState() == Notification.State.FINISHED){
			current = null;
		}

		//oldY = newY + 1;
	}

}
