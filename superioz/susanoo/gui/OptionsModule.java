package de.superioz.susanoo.gui;

import de.superioz.susanoo.module.ConfigSaveableWrapper;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.module.spec.ToggleValue;
import de.superioz.susanoo.util.impl.SerializablePair;

import java.util.HashMap;

/**
 * Created on 03.06.2016.
 */
public class OptionsModule implements ConfigSaveable {

	public static final String SECTION_NAME = "Options";

	public ToggleValue hud = new ToggleValue(true);
	public ToggleValue activate = new ToggleValue(true);
	public ToggleValue radar = new ToggleValue(true);
	public ToggleValue fps = new ToggleValue(true);
	public ToggleValue effects = new ToggleValue(true);
	public ToggleValue armor = new ToggleValue(true);
	public ToggleValue susanooCapes = new ToggleValue(true);
	public ToggleValue fastZoom = new ToggleValue(false);
	public ToggleValue teamMates = new ToggleValue(true);

	//
	public OptionsModule(){
	}

	@Override
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigValues(){
		return ConfigSaveableWrapper.buildMap(
				new ConfigSaveableWrapper(hud, "HUD:", 0),
				new ConfigSaveableWrapper(activate, "Activate:", 1),
				new ConfigSaveableWrapper(radar, "Radar:", 2),
				new ConfigSaveableWrapper(fps, "Debug FPS:", 3),
				new ConfigSaveableWrapper(effects, "Effect HUD:", 4),
				new ConfigSaveableWrapper(armor, "Armor HUD:", 5),
				new ConfigSaveableWrapper(susanooCapes, "Override OF Capes:", 6),
				new ConfigSaveableWrapper(fastZoom, "Fast OF Zoom:", 7),
				new ConfigSaveableWrapper(teamMates, "Friends Function:", 8)
		);
	}

	@Override
	public void setConfigValues(HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		if(values == null){
			return;
		}

		hud = (ToggleValue) values.get(0).getSecond();
		activate = (ToggleValue) values.get(1).getSecond();
		radar = (ToggleValue) values.get(2).getSecond();
		fps = (ToggleValue) values.get(3).getSecond();
		effects = (ToggleValue) values.get(4).getSecond();
		armor = (ToggleValue) values.get(5).getSecond();
		susanooCapes = (ToggleValue) values.get(6).getSecond();
		fastZoom = (ToggleValue) values.get(7).getSecond();
		teamMates = (ToggleValue) values.get(8).getSecond();
	}
}
