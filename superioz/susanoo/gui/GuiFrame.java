package de.superioz.susanoo.gui;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created on 04.06.2016.
 */
public abstract class GuiFrame extends JFrame
{
    public GuiFrame()
    {
        this.setupLookAndFeel();
        this.setIcon();
    }

    /**
     * Sets the look and feel
     */
    public void setupLookAndFeel()
    {
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            SwingUtilities.updateComponentTreeUI(this);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Set the icon
     */
    public void setIcon()
    {
        try
        {
            BufferedImage image = ImageIO.read(getClass().getResource("/de/superioz/susanoo/gui/images/icon.jpg"));
            setIconImage(image);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
