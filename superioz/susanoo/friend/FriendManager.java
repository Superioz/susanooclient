package de.superioz.susanoo.friend;

import de.superioz.susanoo.Client;
import de.superioz.susanoo.config.XConfigurationManager;
import de.superioz.susanoo.event.EventHandler;
import de.superioz.susanoo.event.Listener;
import de.superioz.susanoo.event.events.MouseClickEvent;
import de.superioz.susanoo.gui.notification.Notification;
import de.superioz.susanoo.gui.notification.NotificationCenter;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;

import java.util.UUID;

/**
 * Created on 21.06.2016.
 */
public class FriendManager implements Listener {

	@EventHandler
	public void onMouseClick(MouseClickEvent event){
		if(!(event.getType() == MouseClickEvent.Type.MIDDLECLICK)
				|| !Client.options.teamMates.getState()){
			return;
		}

		Entity hit = Client.mc.objectMouseOver.entityHit;
		if(hit == null) return;
		if(!(hit instanceof EntityPlayer)) return;

		EntityPlayer player = (EntityPlayer) hit;
		String name = player.getCommandSenderName();
		UUID uuid = player.getUniqueID();

		boolean result = addFriend(uuid);
		if(!result) removeFriend(uuid);
		String message = result ? "\2477Added \247a" + name + " \2477as your friend." : "\247a" + name + " \247cremoved as friend!";

		NotificationCenter.addMessage(new Notification(message));
	}

	/**
	 * Adds a friend
	 *
	 * @param uuid The uuid
	 */
	public static boolean addFriend(UUID uuid){
		if(!XConfigurationManager.getConfig().friends.contains(uuid)){
			XConfigurationManager.getConfig().friends.add(uuid);
			return true;
		}
		return false;
	}

	/**
	 * Removes a friend
	 *
	 * @param uuid The uuid
	 */
	public static void removeFriend(UUID uuid){
		if(isFriend(uuid)){
			XConfigurationManager.getConfig().friends.remove(uuid);
		}
	}

	/**
	 * Checks if the player is a friend
	 *
	 * @param uuid The uuid
	 * @return The result
	 */
	public static boolean isFriend(UUID uuid){
		return XConfigurationManager.getConfig().friends.contains(uuid);
	}

	public static boolean isFriend(Entity entityIn){
		return isFriend(entityIn.getUniqueID());
	}

	/**
	 * Clears all friends
	 */
	public static boolean clear(){
		if(XConfigurationManager.getConfig().friends.isEmpty()){
			return false;
		}
		XConfigurationManager.getConfig().friends.clear();
		return true;
	}

}
