package de.superioz.susanoo.util;

import java.awt.*;

/**
 * Created on 02.06.2016.
 */
public class Colors
{
    public static final Color BLACK = new Color(0xff000000);
    public static final Color GREEN = new Color(0x34A853);
    public static final Color RED = new Color(0xE50914);

    public static final Color WHITE = new Color(0xffffffff);
    public static final Color LIGHT_RED = new Color(0xffff0000);
    public static final Color LIGHT_GREEN = new Color(0xff00ff00);
    public static final Color ORANGE_0 = Color.ORANGE;
    public static final Color MAGENTA = Color.MAGENTA;
    public static final Color LIGHT_BLUE = new Color(0xff00ace6);
	public static final Color LIST_VALUE = new Color(0x730099);

    public static final int NAMETAG_OUTLINE = 0xffe6e6e6;
    public static final int NAMETAG_BACKGROUND = 0x70000000;

    public static final Color ORANGE_1 = new Color(0xDD4124);
    public static final Color BLUE_1 = new Color(0x00A1F1);

}
