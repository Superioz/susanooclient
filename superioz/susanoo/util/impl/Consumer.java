package de.superioz.susanoo.util.impl;

/**
 * Created on 03.06.2016.
 */
public interface Consumer<T>
{
    void consume(T object);
}
