package de.superioz.susanoo.util.impl;

import java.io.Serializable;

/**
 * Created on 29.05.2016.
 */
public class SerializablePair<T, K> implements Serializable
{
    private T b0;
    private K b1;

    public SerializablePair(T b0, K b1)
    {
        this.b0 = b0;
        this.b1 = b1;
    }

    public T getFirst()
    {
        return b0;
    }

    public void setFirst(T b0)
    {
        this.b0 = b0;
    }

    public K getSecond()
    {
        return b1;
    }

    public void setSecond(K b1)
    {
        this.b1 = b1;
    }
}
