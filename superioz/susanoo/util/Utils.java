package de.superioz.susanoo.util;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import org.lwjgl.opengl.GL11;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 24.06.2016.
 */
public class Utils {

	/**
	 * Rounds the value
	 *
	 * @param value  The value
	 * @param places The places
	 * @return The value
	 */
	public static double round(double value, int places){
		if(places < 0){
			throw new IllegalArgumentException();
		}
		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	/**
	 * Returns if the entity is moving
	 *
	 * @param e The entity
	 * @return The result
	 */
	public static boolean isMoving(Entity e){
		return e.motionX != 0.0D && e.motionZ != 0.0D && e.motionY != 0.0D;
	}

	/**
	 * Checks the block
	 *
	 * @param a0 The state
	 * @param b0 The id
	 * @param c0 The data
	 * @return The result
	 */
	public static boolean checkBlock(IBlockState a0, int b0, int c0){
		return Block.getIdFromBlock(a0.getBlock()) == b0
				&& a0.getBlock().getMetaFromState(a0) == c0;
	}

	/**
	 * Get the entity speed
	 *
	 * @return The double
	 */
	public static double getEntitySpeed(Entity e){
		double bpsX = (e.motionX < 0 ? e.motionX * -1 : e.motionX) * (1000 / 20);
		double bpsZ = (e.motionZ < 0 ? e.motionZ * -1 : e.motionZ) * (1000 / 20);

		return (bpsX + bpsZ) / 2;
	}

	/**
	 * Get the block with position
	 *
	 * @param x The x
	 * @param y The y
	 * @param z The z
	 * @return The block
	 */
	public static Block getBlock(int x, int y, int z){
		return mc.theWorld.getBlockState(new BlockPos(x, y, z)).getBlock();
	}

	/**
	 * Would the arrow hit the entity
	 *
	 * @param arrow  The arrow
	 * @param entity The entity
	 * @return The result
	 */
	public static boolean wouldHitEntity(EntityArrow arrow, Entity entity, boolean distanceCalculating){
		if(arrow.onGround || arrow.ticksInGround > 0 || arrow.isDead){
			return false;
		}

		double arrowMotionX = arrow.motionX,
				arrowMotionY = arrow.motionY,
				arrowMotionZ = arrow.motionZ;
		double arrowPosX = arrow.posX,
				arrowPosY = arrow.posY,
				arrowPosZ = arrow.posZ;
		double gravity = 0.05;
		RenderManager renderManager = mc.getRenderManager();

		for(int i = 0; i < 1000; i++){
			double x = arrowPosX - renderManager.renderPosX,
					y = arrowPosY - renderManager.renderPosY,
					z = arrowPosZ - renderManager.renderPosZ;
			GL11.glVertex3d(x, y, z);
			double distance = mc.thePlayer.getDistance(arrowPosX, arrowPosY, arrowPosZ);

			// Player
			if(distanceCalculating){
				double speed = Utils.getEntitySpeed(entity);

				if(distance <= speed || distance <= 5){
					return true;
				}
			}

			List<Entity> list = mc.theWorld.getEntitiesInAABBexcluding(null,
					new AxisAlignedBB(arrowPosX - 0.5, arrowPosY - 0.5, arrowPosZ - 0.5,
							arrowPosX + 0.5, arrowPosY + 0.5, arrowPosZ + 0.5), null);
			if(list.contains(entity)){
				return true;
			}

			arrowPosX += arrowMotionX * 0.1;
			arrowPosY += arrowMotionY * 0.1;
			arrowPosZ += arrowMotionZ * 0.1;
			arrowMotionX *= 0.999D;
			arrowMotionY *= 0.999D;
			arrowMotionZ *= 0.999D;
			arrowMotionY -= gravity * 0.1;
		}
		return false;
	}

}
