package de.superioz.susanoo.util.module;

import de.superioz.susanoo.Client;
import de.superioz.susanoo.util.Utils;
import de.superioz.susanoo.util.render.RenderUtil;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.Tuple;

import java.awt.*;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

/**
 * Created on 31.05.2016.
 */
public class MinecraftBlockESP {

	private static List<Tuple<Integer, Integer>> BLOCKS = new ArrayList<Tuple<Integer, Integer>>();
	private static final List<BlockPos> POSITIONS = new ArrayList<BlockPos>();

	public MinecraftBlockESP(List<Tuple<Integer, Integer>> blocks){
		BLOCKS.addAll(blocks);
	}

	/**
	 * On update the block pos
	 *
	 * @param p The pos
	 */
	public void onUpdate(BlockPos p){
		if(!POSITIONS.contains(p)){
			POSITIONS.add(p);
		}
		else if(POSITIONS.contains(p) && !hasBlock(p)){
			POSITIONS.remove(p);
		}
	}

	/**
	 * If the player dies (to reset the list)
	 */
	public void onDie(){
		POSITIONS.clear();
	}

	/**
	 * If has block
	 *
	 * @param p The pos
	 * @return The result
	 */
	public boolean hasBlock(BlockPos p){
		IBlockState state = Client.mc.theWorld.getBlockState(p);

		for(Tuple<Integer, Integer> ti : BLOCKS){
			if(Utils.checkBlock(state, ti.getFirst(), ti.getSecond())){
				return true;
			}
		}

		return false;
	}

	/**
	 * Adds a block to the search list
	 *
	 * @param id   The id
	 * @param data The data
	 */
	public void addBlock(int id, int data){
		BLOCKS.add(new Tuple<Integer, Integer>(id, data));
	}

	/**
	 * On block esp render
	 *
	 * @param color     The render color
	 * @param lineWidth The line width
	 */
	public void onRender(Color color, float lineWidth){
		List<BlockPos> l = POSITIONS;

		try{
			for(BlockPos bp : l){
				if(!hasBlock(bp)){
					continue;
				}

				AxisAlignedBB box = RenderUtil.getOutline(bp);
				RenderUtil.outline(box, color, lineWidth);
			}
		}
		catch(ConcurrentModificationException ex){
			//
		}
	}

	// Intern methid

	public List<BlockPos> getPositions(){
		return POSITIONS;
	}
}
