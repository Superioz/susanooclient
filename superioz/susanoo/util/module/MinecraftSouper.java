package de.superioz.susanoo.util.module;

import de.superioz.susanoo.module.mods.combat.AutoSoup;
import de.superioz.susanoo.util.Timer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.server.S09PacketHeldItemChange;

import java.util.ArrayList;
import java.util.List;

import static de.superioz.susanoo.Client.mc;

/**
 * Created on 25.06.2016.
 */
public class MinecraftSouper {

	private State state = State.UNKNOWN;
	private int startSlot;

	private Timer timer = new Timer();
	private boolean whileSouping = false;
	private boolean whileDropping = false;

	private int refillIndex = -1;
	private List<Integer> refillSoups;

	private int recraftIndex = -1;

	public MinecraftSouper(int startSlot){
		this.startSlot = startSlot;
	}

	public void update(){
		// Check if no container is open except the inventory
		if(mc.currentScreen instanceof GuiContainer
				&& !(mc.currentScreen instanceof GuiInventory)){
			return;
		}

		// Check players health
		if(mc.thePlayer.getHealth() >= AutoSoup.health.getCurrent()){
			return;
		}

		// Check state
		if(state == MinecraftSouper.State.FAST_SOUP
				|| state == MinecraftSouper.State.FAST_DROP){
			List<Integer> soupsInHotbar = findSoups(0, 9);

			// Check if any soup was found
			if(soupsInHotbar.isEmpty()){
				state = MinecraftSouper.State.UNKNOWN;
				return;
			}

			if(!soupsInHotbar.isEmpty()){
				if(state == MinecraftSouper.State.FAST_SOUP){
					this.soup(soupsInHotbar);
				}
				else{
					this.drop();
				}
			}
		}
		else if(state == MinecraftSouper.State.REFILL){
			if(!AutoSoup.refill.getState()){
				state = MinecraftSouper.State.UNKNOWN;
				return;
			}

			List<Integer> soupsInInventory = findSoups(9, 36);

			// Check if any soup was found
			if(soupsInInventory.isEmpty()){
				state = MinecraftSouper.State.UNKNOWN;
				return;
			}

			refill(soupsInInventory);
		}
		else if(state == MinecraftSouper.State.RECRAFT){
			/*if(!(recraftIndex >= 0) && !canRecraft()){
				state = State.UNKNOWN;
				return;
			}

			int redMushroom = getSlotOfItem(Item.getItemById(40), 0, 45);
			int brownMushroom = getSlotOfItem(Item.getItemById(39), 0, 45);
			int bowl = getSlotOfItem(Items.bowl, 0, 45);

			recraft(brownMushroom, redMushroom, bowl);*/
		}
		else{
			// Find soup
			List<Integer> soupsInInventory = findSoups(9, 36);
			List<Integer> soupsInHotbar = findSoups(0, 9);

			// Check if any soup was found
			if(soupsInInventory.isEmpty() && soupsInHotbar.isEmpty()){
				//state = State.RECRAFT;
				return;
			}

			if(!soupsInHotbar.isEmpty()){
				state = MinecraftSouper.State.FAST_SOUP;
			}
			else{
				state = MinecraftSouper.State.REFILL;
			}
		}
	}

	/**
	 * Recraft
	 *
	 * @param brownMush Slot id of brown mushroom
	 * @param redMush   Slot id of red mushroom
	 * @param bowl      Slot id of bowl
	 *                  NOT WORKING CAUSE CRAFTING IS BUGGY!!
	 */
	private void recraft(int brownMush, int redMush, int bowl){
		if(recraftIndex == -1){
			recraftIndex = 0;
			timer.reset();
			return;
		}
		if(recraftIndex > 3){
			timer.reset();
			recraftIndex = -1;
			state = State.UNKNOWN;
			return;
		}

		if(timer.hasReached(40)){
			if(recraftIndex == 0){
				mc.playerController.windowClick(0, brownMush, 0, 0, mc.thePlayer);
				mc.playerController.windowClick(1, 1, 0, 0, mc.thePlayer);
			}
			else if(recraftIndex == 1){
				mc.playerController.windowClick(0, redMush, 0, 0, mc.thePlayer);
				mc.playerController.windowClick(1, 2, 0, 0, mc.thePlayer);
			}
			else if(recraftIndex == 2){
				mc.playerController.windowClick(0, bowl, 0, 0, mc.thePlayer);
				mc.playerController.windowClick(1, 3, 0, 0, mc.thePlayer);
			}
			else if(recraftIndex == 3){
				mc.playerController.windowClick(2, 0, 0, 1, mc.thePlayer);
			}

			timer.reset();
			recraftIndex++;
		}
	}

	/**
	 * Get slot of item
	 *
	 * @param item      The item
	 * @param startSlot The start slot for searching
	 * @param endSlot   The end slot for searching
	 * @return The slot
	 */
	private int getSlotOfItem(Item item, int startSlot, int endSlot){
		if(!mc.thePlayer.inventory.hasItem(item)) return -1;

		for(int i = startSlot; i < endSlot; i++){
			ItemStack stack = mc.thePlayer.inventoryContainer.getSlot(i).getStack();
			if(stack == null) continue;

			if(stack.getItem() == item){
				return i;
			}
		}
		return -1;
	}

	/**
	 * Checks if the player can recraft
	 *
	 * @return The result
	 */
	private boolean canRecraft(){
		boolean redMushroom = mc.thePlayer.inventory.hasItem(Item.getItemById(40));
		boolean brownMushroom = mc.thePlayer.inventory.hasItem(Item.getItemById(39));
		boolean bowl = mc.thePlayer.inventory.hasItem(Items.bowl);

		return redMushroom && brownMushroom && bowl;
	}

	/**
	 * Refills
	 *
	 * @param slots The slots
	 */
	private void refill(final List<Integer> slots){
		if(refillSoups == null){
			refillSoups = slots;
		}
		if(refillIndex == -1){
			refillIndex = 0;
		}
		if(refillIndex >= refillSoups.size() || refillIndex >= 8){
			mc.thePlayer.sendQueue.handleHeldItemChange(new S09PacketHeldItemChange(startSlot));
			mc.playerController.updateController();

			refillIndex = 0;
			state = State.UNKNOWN;
			refillSoups = null;
			return;
		}

		if(timer.hasReached(AutoSoup.refillDelay.getCurrent())){
			int slot = refillSoups.get(refillIndex);
			mc.playerController.windowClick(0, slot, 0, 1, mc.thePlayer);

			refillIndex++;
		}
	}

	/**
	 * Drops a slot
	 */
	private void drop(){
		if(!whileDropping){
			whileDropping = true;
		}

		if(timer.hasReached(AutoSoup.soupDelay.getCurrent())){
			mc.thePlayer.dropOneItem(true);
			mc.thePlayer.sendQueue.handleHeldItemChange(new S09PacketHeldItemChange(0));
			mc.playerController.updateController();

			whileDropping = false;
			state = State.UNKNOWN;
		}
	}

	/**
	 * Soup a soup
	 */
	private void soup(List<Integer> soupsInHotbar){
		if(!whileSouping){
			mc.thePlayer.sendQueue.handleHeldItemChange(new S09PacketHeldItemChange(soupsInHotbar.get(0)));
			mc.playerController.updateController();

			whileSouping = true;
			timer.reset();
		}

		if(timer.hasReached(AutoSoup.soupDelay.getCurrent())){
			mc.thePlayer.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(mc.thePlayer.getCurrentEquippedItem()));
			mc.playerController.updateController();

			state = State.FAST_DROP;
			whileSouping = false;
			timer.reset();
		}
	}

	/**
	 * Find soups with startSlot and endSlot
	 *
	 * @param startSlot The startSlot
	 * @param endSlot   The end startSlot
	 * @return The list
	 */
	private List<Integer> findSoups(int startSlot, int endSlot){
		List<Integer> slots = new ArrayList<>();

		for(int i = startSlot; i < endSlot; i++){
			if(i >= mc.thePlayer.inventory.mainInventory.length){
				continue;
			}
			ItemStack stack = mc.thePlayer.inventory.getStackInSlot(i);
			if(stack == null) continue;

			if(stack.getItem() == Items.mushroom_stew){
				slots.add(i);
			}
		}
		return slots;
	}

	// Intern

	public State getState(){
		return state;
	}

	public int getStartSlot(){
		return startSlot;
	}

	public enum State {

		UNKNOWN,
		FAST_SOUP,
		FAST_DROP,
		REFILL,
		RECRAFT

	}

}
