package de.superioz.susanoo.util.module;

import de.superioz.susanoo.friend.FriendManager;
import de.superioz.susanoo.module.ModuleManager;
import de.superioz.susanoo.module.mods.combat.SmoothAimbot;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;

/**
 * Created on 01.06.2016.
 */
public class MinecraftAimBot implements Runnable {

	private static Minecraft mc = Minecraft.getMinecraft();
	private static int x = 0;

	@Override
	public void run(){
		// Automatically aims to next enemy
		while(true){
			// Is the module and the screen active?
			if(!ModuleManager.getModule(SmoothAimbot.class).isActive()
					|| mc.currentScreen != null){
				continue;
			}

			// Loop through all entities current loaded
			for(int i = 0; i < mc.theWorld.loadedEntityList.size(); i++){
				Entity e = mc.theWorld.getLoadedEntityList().get(i);

				// Is the entity a valid player? (not dead)
				if(!(e instanceof EntityPlayer)
						|| mc.thePlayer.getHealth() == 0f
						|| mc.thePlayer.isDead){
					continue;
				}
				if(SmoothAimbot.ignoreFriends.getState() && FriendManager.isFriend(e)){
					continue;
				}

				// Is the current target not null but the players is to high
				// or the target is dead, or the mouse is too far away from the player (FOV)
				if((SmoothAimbot.target != null) &&
						((mc.thePlayer.getDistanceToEntity(SmoothAimbot.target) > SmoothAimbot.range.getCurrent())
								|| (SmoothAimbot.target.isDead)
								|| (SmoothAimbot.getDistanceFromMouse(e) > SmoothAimbot.fov.getCurrent() / 2))){
					SmoothAimbot.target = null;
				}

				// Is the entity of this list not the player, not dead, visible
				// and is the players between player and entity and mouse (FOV) and entity correct
				if((e != mc.thePlayer) && !(e.isDead) && !(e.isInvisible())
						&& (mc.thePlayer.getDistanceToEntity(e) <= SmoothAimbot.range.getCurrent())
						&& (mc.thePlayer.canEntityBeSeen(e))
						&& (SmoothAimbot.getDistanceFromMouse(e) < SmoothAimbot.fov.getCurrent() / 2)){
					if(SmoothAimbot.target == null){
						SmoothAimbot.target = e;
					}

					if(e == SmoothAimbot.target){
						SmoothAimbot.faceEntity(SmoothAimbot.target);
					}
				}
			}

			if(x > 10){
				try{
					Thread.sleep(1L);
					x = 0;
				}
				catch(InterruptedException e){
					e.printStackTrace();
				}
			}
			else{
				x += 1;
			}
		}
	}
}
