package de.superioz.susanoo.util.render;

import de.superioz.susanoo.Client;
import de.superioz.susanoo.friend.FriendManager;
import de.superioz.susanoo.util.Colors;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created on 03.06.2016.
 */
public class NameTagManager {

	private static final int DISPLAY_OFFSET = 8;
	private static final int HAND_ITEM_OFFSET = 20;
	private static final int ARMOR_OFFSET = 15;
	private static final int DISPLAY_ZLEVEL = -100;
	private static final Minecraft minecraft = Client.mc;

	/**
	 * Renders the nametag for given entity
	 *
	 * @param entity The player
	 * @param tag    The nametag
	 * @param pX     The position X
	 * @param pY     The position Y
	 * @param pZ     The position Z
	 */
	public static void renderNameTag(EntityPlayer entity, String tag, double pX, double pY, double pZ, boolean a, boolean h){
		FontRenderer fr = minecraft.fontRendererObj;
		RenderManager renderManager = minecraft.getRenderManager();
		float distance = minecraft.thePlayer.getDistanceToEntity(entity) / 4.0F;
		int color = 16777215;

		// Distance and height
		if(distance < 1.6F){
			distance = 1.6F;
		}

		pY += (entity.isSneaking() ? 0.5D : 0.7D);
		// Health
		double health = Math.ceil(entity.getHealth() + entity.getAbsorptionAmount()) / 2.0D;
		String healthStr = getHealthString(health);
		int healthCol = getHealthColor(entity, health);

		// Scale / Width
		float scale = distance / 80.0F;
		int width = minecraft.fontRendererObj.getStringWidth(tag + (h ? " " + healthStr : "")) / 2;

		// GL
		GL11.glPushMatrix();
		GL11.glTranslatef((float) pX, (float) pY + 1.4F, (float) pZ);
		GL11.glNormal3f(0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
		GL11.glScalef(-scale, -scale, scale);
		GLUtil.setGLCap(GL11.GL_LIGHTING, false);
		GLUtil.setGLCap(GL11.GL_DEPTH_TEST, false);
		GLUtil.setGLCap(GL11.GL_BLEND, true);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		// Background
		float posMinX = -width - 2, posMaxX = width + 2;
		float posMinY = -(minecraft.fontRendererObj.FONT_HEIGHT + 1), posMaxY = 4.0F;

		boolean isFriend = FriendManager.isFriend(entity);

		RenderHelper.drawBorderedRect(posMinX, posMinY, posMaxX, posMaxY, 1.3F,
				isFriend ? new Color(0xff00b300).getRGB() : Colors.NAMETAG_OUTLINE, Colors.NAMETAG_BACKGROUND);
		fr.drawString(tag, -width, -(minecraft.fontRendererObj.FONT_HEIGHT - 1) + 1, color, true);

		// Health
		if(h){
			fr.drawString(healthStr, -width + minecraft.fontRendererObj.getStringWidth(tag + " "),
					-(minecraft.fontRendererObj.FONT_HEIGHT - 1) + 1, healthCol, true);
		}

		GL11.glPushMatrix();

		// Render Armor
		if(a){
			renderArmor(entity);
		}

		// GL11
		GL11.glPopMatrix();
		GLUtil.revertAllCaps();
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glPopMatrix();
	}

	/**
	 * Renders armor
	 *
	 * @param entity The entity
	 */
	public static void renderArmor(EntityPlayer entity){
		int xOffset = 0;
		// Offset
		ItemStack[] arrayOfItemStack;
		int j = (arrayOfItemStack = entity.inventory.armorInventory).length;

		for(int i = 0; i < j; i++){
			ItemStack armourStack = arrayOfItemStack[i];

			if(armourStack != null){
				xOffset -= DISPLAY_OFFSET;
			}
		}

		// Item In Hand
		if(entity.getHeldItem() != null){
			xOffset -= DISPLAY_OFFSET;
			ItemStack renderStack = entity.getHeldItem().copy();

			if(renderStack.hasEffect() && (renderStack.getItem() instanceof ItemTool
					|| renderStack.getItem() instanceof ItemArmor)){
				renderStack.stackSize = 1;
			}

			renderItemStack(renderStack, xOffset, -28);
			xOffset += HAND_ITEM_OFFSET;
		}

		ItemStack[] arrayOfItemStack2;
		int k = (arrayOfItemStack2 = entity.inventory.armorInventory).length;

		// Armor
		for(j = k - 1; j >= 0; j--){
			ItemStack armourStack = arrayOfItemStack2[j];

			if(armourStack != null){
				ItemStack renderStack1 = armourStack.copy();

				if((renderStack1.hasEffect())
						&& (((renderStack1.getItem() instanceof ItemTool))
						|| ((renderStack1.getItem() instanceof ItemArmor)))){
					renderStack1.stackSize = 1;
				}

				renderItemStack(renderStack1, xOffset, -28);
				xOffset += ARMOR_OFFSET;
			}
		}
	}

	/**
	 * Renders the itemstack
	 *
	 * @param stack The item
	 * @param x     The posX
	 * @param y     The posY
	 */
	public static void renderItemStack(ItemStack stack, int x, int y){
		GL11.glPushMatrix();
		GL11.glDepthMask(true);
		GlStateManager.clear(GL11.GL_ACCUM);
		RenderHelper.enableStandardItemLighting();
		// Renders Item
		minecraft.getRenderItem().zLevel = DISPLAY_ZLEVEL;
		fixItemGLint();
		minecraft.getRenderItem().renderItemIntoGUI(stack, x, y);
		minecraft.getRenderItem().zLevel = 0.0F;
		// GL / Normalizee
		RenderHelper.disableStandardItemLighting();
		GlStateManager.disableCull();
		GlStateManager.enableAlpha();
		GlStateManager.disableBlend();
		GlStateManager.disableLighting();
		GlStateManager.enableDepth();
		GlStateManager.scale(2.0F, 2.0F, 2.0F);
		GL11.glPopMatrix();
	}

	/**
	 * Get the string for the health
	 *
	 * @param health The health
	 * @return The string
	 */
	public static String getHealthString(double health){
		String healthStr;

		if(Math.floor(health) == health){
			healthStr = String.valueOf((int) Math.floor(health));
		}
		else{
			healthStr = String.valueOf(health);
		}

		return healthStr;
	}

	/**
	 * Get the health color for given entityPlayer
	 *
	 * @param player The player
	 * @return The color
	 */
	public static int getHealthColor(EntityPlayer player, double health){
		int healthCol;

		if((player.getAbsorptionAmount() > 0.0F)
				&& (player.getHealth() + player.getAbsorptionAmount() >= player.getMaxHealth())){
			healthCol = 5635925;
		}
		else{
			switch((int) Math.floor(health * 2.0D)){
				case 13:
				case 14:
				case 15:
				case 16:
				case 17:
				case 18:
				case 19:
				case 20:
					healthCol = 5635925;
					break;

				case 8:
				case 9:
				case 10:
				case 12:
					healthCol = 16777045;
					break;

				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
					healthCol = 16733525;
					break;

				case 11:
				default:
					healthCol = 5635925;
			}
		}

		return healthCol;
	}

	/**
	 * Fixes GLint thingy
	 */
	public static void fixItemGLint(){
		GlStateManager.disableLighting();
		GlStateManager.disableDepth();
		GlStateManager.disableBlend();
		GlStateManager.enableLighting();
		GlStateManager.enableDepth();
		GlStateManager.disableLighting();
		GlStateManager.disableDepth();
		GlStateManager.disableTexture2D();
		GlStateManager.disableAlpha();
		GlStateManager.disableBlend();
		GlStateManager.enableBlend();
		GlStateManager.enableAlpha();
		GlStateManager.enableTexture2D();
		GlStateManager.enableLighting();
		GlStateManager.enableDepth();
	}

	/**
	 * Created on 02.06.2016.
	 */
	public static class GLUtil {

		private static Map<Integer, Boolean> glCapMap = new HashMap<Integer, Boolean>();

		public static void setGLCap(int cap, boolean flag){
			glCapMap.put(cap, GL11.glGetBoolean(cap));

			if(flag){
				GL11.glEnable(cap);
			}
			else{
				GL11.glDisable(cap);
			}
		}

		public static void revertGLCap(int cap){
			Boolean origCap = (Boolean) glCapMap.get(cap);

			if(origCap != null){
				if(origCap){
					GL11.glEnable(cap);
				}
				else{
					GL11.glDisable(cap);
				}
			}
		}

		public static void revertAllCaps(){
			for(Iterator localIterator = glCapMap.keySet().iterator(); localIterator.hasNext(); ){
				int cap = (Integer) localIterator.next();
				revertGLCap(cap);
			}
		}
	}
}
