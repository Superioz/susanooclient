package de.superioz.susanoo.util.render;

import de.superioz.susanoo.Client;
import de.superioz.susanoo.module.mods.render.Trajectories;
import de.superioz.susanoo.util.Colors;
import de.superioz.susanoo.util.impl.Consumer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.item.*;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static de.superioz.susanoo.Client.mc;
import static de.superioz.susanoo.module.mods.render.Trajectories.entityStack;

/**
 * Created on 26.05.2016.
 */
public class RenderUtil {

	private static Tessellator tessellator;

	public static Tessellator getTessellator(){
		if(tessellator == null){
			tessellator = Tessellator.getInstance();
		}

		return tessellator;
	}

	public static WorldRenderer getRenderer(){
		return getTessellator().getWorldRenderer();
	}

	/**
	 * Render trajectories for given motions
	 *
	 * @param arrowMotionX  The x
	 * @param arrowMotionY  The y
	 * @param arrowMotionZ  The z
	 * @param arrowPosX     The x2
	 * @param arrowPosY     The y2
	 * @param arrowPosZ     The z2
	 * @param gravity       The gravity
	 * @param hitConsumer   The hit consumer
	 * @param drawEndOfLine The end of l
	 * @param drawHitTarget Hit the target Draw
	 * @return The result
	 */
	public static boolean renderTrajectories(double arrowMotionX,
	                                         double arrowMotionY, double arrowMotionZ,
	                                         double arrowPosX, double arrowPosY,
	                                         double arrowPosZ,
	                                         double gravity,
	                                         Consumer<List<Entity>> hitConsumer,
	                                         boolean drawEndOfLine,
	                                         boolean drawHitTarget){
		// GL settings
		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL13.GL_MULTISAMPLE);
		GL11.glDepthMask(false);
		GL11.glLineWidth(1.2F);

		//
		RenderManager renderManager = mc.getRenderManager();
		EntityPlayerSP player = mc.thePlayer;
		Vec3 playerVector = new Vec3(player.posX, player.posY + player.getEyeHeight(), player.posZ);
		List<Entity> entityList = new ArrayList<>();

		//
		GL11.glColor3d(1, 1, 1);
		GL11.glBegin(GL11.GL_LINE_STRIP);
		for(int i = 0; i < 1000; i++){
			double x = arrowPosX - renderManager.renderPosX,
					y = arrowPosY - renderManager.renderPosY,
					z = arrowPosZ - renderManager.renderPosZ;
			GL11.glVertex3d(x, y, z);

			List<Entity> list = mc.theWorld.getEntitiesInAABBexcluding(mc.thePlayer,
					new AxisAlignedBB(arrowPosX - 0.5, arrowPosY - 0.5, arrowPosZ - 0.5,
							arrowPosX + 0.5, arrowPosY + 0.5, arrowPosZ + 0.5), null);
			hitConsumer.consume(list);
			for(Entity e : list){
				if(!e.isDead && e instanceof EntityLivingBase && e != mc.thePlayer){
					entityList.add(e);
				}
			}

			arrowPosX += arrowMotionX * 0.1;
			arrowPosY += arrowMotionY * 0.1;
			arrowPosZ += arrowMotionZ * 0.1;
			arrowMotionX *= 0.999D;
			arrowMotionY *= 0.999D;
			arrowMotionZ *= 0.999D;
			arrowMotionY -= gravity * 0.1;

			if(mc.theWorld.rayTraceBlocks(playerVector, new Vec3(arrowPosX, arrowPosY, arrowPosZ)) != null){
				break;
			}
		}
		GL11.glEnd();

		if(drawEndOfLine){
			// Draw end of trajectory line
			double renderX = arrowPosX - renderManager.renderPosX;
			double renderY = arrowPosY - renderManager.renderPosY;
			double renderZ = arrowPosZ - renderManager.renderPosZ;
			AxisAlignedBB bb = new AxisAlignedBB(
					renderX - 0.5, renderY - 0.5, renderZ - 0.5,
					renderX + 0.5, renderY + 0.5, renderZ + 0.5);
			RenderUtil.outline(bb, new Color(1f, 1f, 1f), 0.55f);
		}
		if(drawHitTarget){
			entityStack.clear();
			for(Entity e : entityList){
				entityStack.add(e.getEntityId());
			}
		}

		// GL resets
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glDisable(GL13.GL_MULTISAMPLE);
		GL11.glDepthMask(true);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
		GL11.glPopMatrix();
		return true;
	}

	public static void renderTrajectories(float partialTicks, boolean firstPerson){
		if(firstPerson){
			EntityPlayerSP player = mc.thePlayer;
			boolean usingBow = player.inventory.getCurrentItem().getItem() instanceof ItemBow;

			// Check if player is holding an item
			ItemStack stack = player.inventory.getCurrentItem();
			if(stack == null){
				return;
			}
			Item item = stack.getItem();

			// Starting position
			double arrowPosX = player.lastTickPosX + (player.posX - player.lastTickPosX)
					* partialTicks
					- MathHelper.cos((float) Math.toRadians(player.rotationYaw))
					* 0.16F;
			double arrowPosY = player.lastTickPosY + (player.posY - player.lastTickPosY)
					* partialTicks
					+ player.getEyeHeight() - 0.1;
			double arrowPosZ = player.lastTickPosZ + (player.posZ - player.lastTickPosZ)
					* partialTicks
					- MathHelper.sin((float) Math.toRadians(player.rotationYaw))
					* 0.16F;

			// Starting motion
			float arrowMotionFactor = usingBow ? 1F : 0.4F;
			float yaw = (float) Math.toRadians(player.rotationYaw);
			float pitch = (float) Math.toRadians(player.rotationPitch);

			float arrowMotionX =
					-MathHelper.sin(yaw) * MathHelper.cos(pitch) * arrowMotionFactor;
			float arrowMotionY =
					-MathHelper.sin(pitch) * arrowMotionFactor;
			float arrowMotionZ =
					MathHelper.cos(yaw) * MathHelper.cos(pitch) * arrowMotionFactor;
			double arrowMotion =
					Math.sqrt(arrowMotionX * arrowMotionX + arrowMotionY * arrowMotionY
							+ arrowMotionZ * arrowMotionZ);
			arrowMotionX /= arrowMotion;
			arrowMotionY /= arrowMotion;
			arrowMotionZ /= arrowMotion;

			// Motion with bow power
			if(usingBow){
				float bowPower = (72000 - player.getItemInUseCount()) / 20F;
				bowPower = (bowPower * bowPower + bowPower * 2F) / 3F;

				if(bowPower > 1F)
					bowPower = 1F;

				if(bowPower <= 0.1F)
					bowPower = 1F;

				bowPower *= 3F;
				arrowMotionX *= bowPower;
				arrowMotionY *= bowPower;
				arrowMotionZ *= bowPower;
			}
			else{
				arrowMotionX *= 1.5D;
				arrowMotionY *= 1.5D;
				arrowMotionZ *= 1.5D;
			}


			double gravity = usingBow ? 0.05D : item instanceof ItemPotion ? 0.4D : item instanceof ItemFishingRod ? 0.15D : 0.03D;
			final java.util.List<Entity> entityList = new ArrayList<>();

			renderTrajectories(arrowMotionX, arrowMotionY, arrowMotionZ,
					arrowPosX, arrowPosY, arrowPosZ, gravity, new Consumer<List<Entity>>() {
						@Override
						public void consume(List<Entity> object){
							for(Entity e : object){
								if(!e.isDead && e instanceof EntityLivingBase && e != mc.thePlayer){
									entityList.add(e);
								}
							}

							if(entityList.size() > 0){
								Color c = Colors.ORANGE_0;
								GL11.glColor4d(((double) c.getRed() / 255) * 100, ((double) c.getGreen() / 255) * 100,
										((double) c.getBlue() / 255) * 100, 0.55);
							}
							else{
								GL11.glColor4d(1, 1, 1, 0.55);
							}
						}
					}, true, Trajectories.highlightTarget.getState());
		}
		else{
			List<Entity> entityList = mc.theWorld.loadedEntityList;
			List<EntityArrow> arrows = new ArrayList<>();

			for(Entity e : entityList){
				if(e instanceof EntityArrow) arrows.add((EntityArrow) e);
			}

			for(EntityArrow arrow : arrows){
				if(arrow.onGround || arrow.ticksInGround > 0){
					continue;
				}

				// Arrow motion
				double arrowMotionX = arrow.motionX,
						arrowMotionY = arrow.motionY,
						arrowMotionZ = arrow.motionZ;

				// Arrow position
				double arrowPosX = arrow.posX,
						arrowPosY = arrow.posY,
						arrowPosZ = arrow.posZ;
				double gravity = 0.05;

				renderTrajectories(arrowMotionX, arrowMotionY, arrowMotionZ,
						arrowPosX, arrowPosY, arrowPosZ, gravity, new Consumer<List<Entity>>() {
							@Override
							public void consume(List<Entity> object){
								// Empty
							}
						}, false, true);
			}
		}
	}

	/**
	 * Get the outline axis of this block
	 *
	 * @param bp The block
	 * @return The axis object
	 */
	public static AxisAlignedBB getOutline(BlockPos bp){
		double rX = Client.mc.getRenderManager().renderPosX;
		double rY = Client.mc.getRenderManager().renderPosY;
		double rZ = Client.mc.getRenderManager().renderPosZ;
		double minX = bp.getX() - rX, maxX = minX + 1;
		double minY = bp.getY() - rY, maxY = minY + 1;
		double minZ = bp.getZ() - rZ, maxZ = minZ + 1;
		return new AxisAlignedBB(minX, minY, minZ, maxX, maxY, maxZ);
	}

	/**
	 * Get the axis of the entity
	 *
	 * @param e The entity
	 * @return The axis
	 */
	public static AxisAlignedBB getOutline(Entity e){
		AxisAlignedBB b = e.getEntityBoundingBox();

		double rX = Client.mc.getRenderManager().renderPosX;
		double rY = Client.mc.getRenderManager().renderPosY;
		double rZ = Client.mc.getRenderManager().renderPosZ;
		double minX = b.minX - rX, maxX = b.maxX - rX;
		double minY = b.minY - rY, maxY = b.maxY - rY;
		double minZ = b.minZ - rZ, maxZ = b.maxZ - rZ;
		return new AxisAlignedBB(minX, minY, minZ, maxX, maxY, maxZ);
	}

	/**
	 * Outlines border from given axis
	 *
	 * @param aa        The axis object
	 * @param color     The color
	 * @param lineWidth The line width
	 */
	public static void outline(AxisAlignedBB aa, java.awt.Color color, float lineWidth){
		prepareGL();
		GL11.glColor4f(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
		GL11.glLineWidth(lineWidth);
		Outline.onRender(aa);
		disperseGL();
	}

	/**
	 * Fill borders from given axis
	 *
	 * @param aa        The axis object
	 * @param color     The color
	 * @param lcolor    The outline color
	 * @param lineWidth The line width
	 */
	public static void fill(AxisAlignedBB aa, java.awt.Color color, java.awt.Color lcolor, float lineWidth){
		prepareGL();
		GL11.glColor4f(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
		GL11.glLineWidth(lineWidth);
		Solid.onRender(aa);
		GL11.glColor4f(lcolor.getRed(), lcolor.getGreen(), lcolor.getBlue(), lcolor.getAlpha());
		Outline.onRender(aa);
		disperseGL();
	}

	/**
	 * Draws a line between player and given coords
	 *
	 * @param x         The x-coord of target
	 * @param y         The y-coord of target
	 * @param z         The z-coord of target
	 * @param color     The color of the line
	 * @param lineWidth The width of the line
	 */
	public static void trace(double x, double y, double z, java.awt.Color color, float lineWidth){
		prepareGL();
		GL11.glColor4f(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
		GL11.glLineWidth(lineWidth);
		Tracer.onRender(x, y, z);
		disperseGL();
	}

	/**
	 * Activates important open gl values
	 */
	public static void prepareGL(){
		// Push buffer
		GL11.glPushMatrix();
		// Using OpenGL
		// GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glDepthMask(false);
	}

	/**
	 * Deactivates used open gl values
	 */
	public static void disperseGL(){
		// Using OpenGL
		// GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glDepthMask(true);
		// Pop buffer
		GL11.glPopMatrix();
	}

	public static class Outline {

		public static void onRender(AxisAlignedBB aa){
			Tessellator tessellator = getTessellator();
			WorldRenderer renderer = getRenderer();
			renderer.startDrawing(GL11.GL_LINE_STRIP);
			renderer.addVertex(aa.minX, aa.minY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.minY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.minY, aa.maxZ);
			renderer.addVertex(aa.minX, aa.minY, aa.maxZ);
			renderer.addVertex(aa.minX, aa.minY, aa.minZ);
			tessellator.draw();
			renderer.startDrawing(GL11.GL_LINE_STRIP);
			renderer.addVertex(aa.minX, aa.maxY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.maxY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.maxY, aa.maxZ);
			renderer.addVertex(aa.minX, aa.maxY, aa.maxZ);
			renderer.addVertex(aa.minX, aa.maxY, aa.minZ);
			tessellator.draw();
			renderer.startDrawing(GL11.GL_LINES);
			renderer.addVertex(aa.minX, aa.minY, aa.minZ);
			renderer.addVertex(aa.minX, aa.maxY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.minY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.maxY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.minY, aa.maxZ);
			renderer.addVertex(aa.maxX, aa.maxY, aa.maxZ);
			renderer.addVertex(aa.minX, aa.minY, aa.maxZ);
			renderer.addVertex(aa.minX, aa.maxY, aa.maxZ);
			tessellator.draw();
		}
	}

	public static class Solid {

		public static void onRender(AxisAlignedBB aa){
			Tessellator tessellator = getTessellator();
			WorldRenderer renderer = getRenderer();
			// 1 & 2
			renderer.startDrawing(GL11.GL_QUADS);
			renderer.addVertex(aa.minX, aa.minY, aa.minZ);
			renderer.addVertex(aa.minX, aa.maxY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.minY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.maxY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.minY, aa.maxZ);
			renderer.addVertex(aa.maxX, aa.maxY, aa.maxZ);
			renderer.addVertex(aa.minX, aa.minY, aa.maxZ);
			renderer.addVertex(aa.minX, aa.maxY, aa.maxZ);
			tessellator.draw();
			// 3 & 4
			renderer.startDrawing(GL11.GL_QUADS);
			renderer.addVertex(aa.maxX, aa.maxY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.minY, aa.minZ);
			renderer.addVertex(aa.minX, aa.maxY, aa.minZ);
			renderer.addVertex(aa.minX, aa.minY, aa.minZ);
			renderer.addVertex(aa.minX, aa.maxY, aa.maxZ);
			renderer.addVertex(aa.minX, aa.minY, aa.maxZ);
			renderer.addVertex(aa.maxX, aa.maxY, aa.maxZ);
			renderer.addVertex(aa.maxX, aa.minY, aa.maxZ);
			tessellator.draw();
			// 5 & 6
			renderer.startDrawing(GL11.GL_QUADS);
			renderer.addVertex(aa.minX, aa.maxY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.maxY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.maxY, aa.maxZ);
			renderer.addVertex(aa.minX, aa.maxY, aa.maxZ);
			renderer.addVertex(aa.minX, aa.maxY, aa.minZ);
			renderer.addVertex(aa.minX, aa.maxY, aa.maxZ);
			renderer.addVertex(aa.maxX, aa.maxY, aa.maxZ);
			renderer.addVertex(aa.maxX, aa.maxY, aa.minZ);
			tessellator.draw();
			// 7 & 8
			renderer.startDrawing(GL11.GL_QUADS);
			renderer.addVertex(aa.minX, aa.minY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.minY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.minY, aa.maxZ);
			renderer.addVertex(aa.minX, aa.minY, aa.maxZ);
			renderer.addVertex(aa.minX, aa.minY, aa.minZ);
			renderer.addVertex(aa.minX, aa.minY, aa.maxZ);
			renderer.addVertex(aa.maxX, aa.minY, aa.maxZ);
			renderer.addVertex(aa.maxX, aa.minY, aa.minZ);
			tessellator.draw();
			// 9 & 10
			renderer.startDrawing(GL11.GL_QUADS);
			renderer.addVertex(aa.minX, aa.minY, aa.minZ);
			renderer.addVertex(aa.minX, aa.maxY, aa.minZ);
			renderer.addVertex(aa.minX, aa.minY, aa.maxZ);
			renderer.addVertex(aa.minX, aa.maxY, aa.maxZ);
			renderer.addVertex(aa.maxX, aa.minY, aa.maxZ);
			renderer.addVertex(aa.maxX, aa.maxY, aa.maxZ);
			renderer.addVertex(aa.maxX, aa.minY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.maxY, aa.minZ);
			tessellator.draw();
			// 11 & 12
			renderer.startDrawing(GL11.GL_QUADS);
			renderer.addVertex(aa.minX, aa.maxY, aa.maxZ);
			renderer.addVertex(aa.minX, aa.minY, aa.maxZ);
			renderer.addVertex(aa.minX, aa.maxY, aa.minZ);
			renderer.addVertex(aa.minX, aa.minY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.maxY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.minY, aa.minZ);
			renderer.addVertex(aa.maxX, aa.maxY, aa.maxZ);
			renderer.addVertex(aa.maxX, aa.minY, aa.maxZ);
			tessellator.draw();
		}
	}

	static class Tracer {

		static void onRender(double x, double y, double z){
			GL11.glBegin(GL11.GL_LINE_LOOP);
			GL11.glVertex3d(0.0D, Minecraft.getMinecraft().thePlayer.getEyeHeight(), 0.0D);
			GL11.glVertex3d(x, y, z);
			GL11.glEnd();
		}
	}
}
