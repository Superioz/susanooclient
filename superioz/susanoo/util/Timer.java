package de.superioz.susanoo.util;

/**
 * Created on 01.06.2016.
 */
public class Timer {

	private long lastMS;

	/**
	 * Get the current milli seoncds
	 *
	 * @return The long
	 */
	public long getCurrent(){
		return System.currentTimeMillis();
	}

	/**
	 * Get the last milliseconds
	 *
	 * @return The long
	 */
	public long getLast(){
		return this.lastMS;
	}

	/**
	 * Checks if the milliseconds
	 *
	 * @param milliseconds The milliseconds
	 * @return The result
	 */
	public boolean hasReached(long milliseconds){
		return getTimeSinceReset() >= milliseconds;
	}

	/**
	 * Resets the last milliseconds
	 */
	public void reset(){
		this.setLastMS(getCurrent());
	}

	/**
	 * Set the last milliseconds
	 *
	 * @param currentMS The current long
	 */
	public void setLastMS(long currentMS){
		this.lastMS = currentMS;
	}

	/**
	 * Get the time since the last ms
	 *
	 * @return The long
	 */
	public long getTimeSinceReset(){
		return getCurrent() - this.lastMS;
	}
}
