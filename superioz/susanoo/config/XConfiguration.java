package de.superioz.susanoo.config;

import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.util.impl.SerializablePair;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created on 27.05.2016.
 */
public class XConfiguration implements Serializable {

	public HashMap<String, Integer> keyCodes = new HashMap<String, Integer>();
	public HashMap<String, HashMap<Integer, SerializablePair<String, ConfigValue>>> configMap
			= new HashMap<String, HashMap<Integer, SerializablePair<String, ConfigValue>>>();
	public List<UUID> friends = new ArrayList<>();

	/**
	 * Adds a config section
	 *
	 * @param s      The name as id
	 * @param values The values
	 */
	public void addConfigSection(String s, HashMap<Integer, SerializablePair<String, ConfigValue>> values){
		configMap.put(s, values);
	}

	/**
	 * Gets a config section
	 *
	 * @param s The name
	 * @return The map
	 */
	public HashMap<Integer, SerializablePair<String, ConfigValue>> getConfigSection(String s){
		return configMap.get(s);
	}

	/**
	 * Adds a friend
	 *
	 * @param uuid The uuid
	 */
	public boolean addFriend(UUID uuid){
		if(!friends.contains(uuid)){
			friends.add(uuid);
			return true;
		}
		return false;
	}

	/**
	 * Removes a friend
	 *
	 * @param uuid The uuid
	 */
	public void removeFriend(UUID uuid){
		if(friends.contains(uuid)) friends.remove(uuid);
	}

}
