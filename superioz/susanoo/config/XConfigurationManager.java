package de.superioz.susanoo.config;

import de.superioz.susanoo.Client;
import de.superioz.susanoo.gui.OptionsModule;
import de.superioz.susanoo.module.Module;
import de.superioz.susanoo.module.ModuleManager;
import de.superioz.susanoo.module.spec.ConfigSaveable;
import de.superioz.susanoo.module.spec.ConfigValue;
import de.superioz.susanoo.util.impl.SerializablePair;

import java.io.*;
import java.util.HashMap;

/**
 * Created on 27.05.2016.
 */
public class XConfigurationManager {

	public static final String NAME = "config";
	public static final String SUFFIX = ".cfg";

	private static XConfiguration config = new XConfiguration();
	private static File file;

	/**
	 * Set the configuration up
	 */
	public static void setup(){
		getFile();
		getConfig();
		read();

		HashMap<Integer, SerializablePair<String, ConfigValue>> section = config.getConfigSection(OptionsModule.SECTION_NAME);
		if(section != null)
			Client.options.setConfigValues(section);

		for(Module m : ModuleManager.getModules()){
			if(!config.keyCodes.containsKey(m.getName())){
				config.keyCodes.put(m.getName(), m.getKeyCode());
			}

			m.setKeyCode(config.keyCodes.get(m.getName()));

			if(m instanceof ConfigSaveable){
				ConfigSaveable saveable = (ConfigSaveable) m;

				HashMap<Integer, SerializablePair<String, ConfigValue>> map = config.getConfigSection(m.getName());

				if(map != null)
					saveable.setConfigValues(config.getConfigSection(m.getName()));
			}
		}
	}

	/**
	 * Saves the configuration
	 */
	public static void shutdown(){
		config.addConfigSection(OptionsModule.SECTION_NAME, Client.options.getConfigValues());

		for(Module m : ModuleManager.getModules()){
			if(config.keyCodes.containsKey(m.getName())){
				config.keyCodes.put(m.getName(), m.getKeyCode());
			}

			if(m instanceof ConfigSaveable){
				if(!m.getName().equalsIgnoreCase(OptionsModule.SECTION_NAME)){
					ConfigSaveable saveable = (ConfigSaveable) m;
					HashMap<Integer, SerializablePair<String, ConfigValue>> map = saveable.getConfigValues();
					config.addConfigSection(m.getName(), map);
				}
			}
		}

		write();
	}

	/**
	 * Get the configuration file
	 *
	 * @return The file
	 */
	public static File getFile(){
		if(file == null){
			file = new File(Client.CLIENT_NAME + "/" + NAME + SUFFIX);
		}

		if(!file.exists()){
			try{
				File folder = new File(Client.CLIENT_NAME);
				folder.mkdirs();
				file.createNewFile();
			}
			catch(IOException e){
				//
			}
		}

		return file;
	}

	/**
	 * Updates the configuration file
	 *
	 * @return The file
	 */
	public static boolean read(){
		ObjectInputStream inputStream = null;

		try{
			inputStream = new ObjectInputStream(new FileInputStream(getFile()));
			config = (XConfiguration) inputStream.readObject();
		}
		catch(FileNotFoundException e){
			return false;
		}
		catch(IOException e){
			return false;
		}
		catch(ClassNotFoundException e){
			return false;
		}
		finally{
			if(inputStream != null){
				try{
					inputStream.close();
				}
				catch(IOException e){
					//
				}
			}
		}

		return true;
	}

	/**
	 * Writes config into binary file
	 *
	 * @return The result
	 */
	public static boolean write(){
		ObjectOutputStream outputStream = null;

		try{
			outputStream = new ObjectOutputStream(new FileOutputStream(getFile()));
			outputStream.writeObject(getConfig());
		}
		catch(FileNotFoundException e){
			return false;
		}
		catch(IOException e){
			return false;
		}
		finally{
			try{
				if(outputStream != null){
					outputStream.close();
				}
			}
			catch(IOException e){
				//
			}
		}

		return true;
	}

	// Intern methods

	public static XConfiguration getConfig(){
		if(config == null){
			config = new XConfiguration();
		}

		return config;
	}
}
